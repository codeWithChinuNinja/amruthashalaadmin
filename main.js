const path = require('path');
const glob = require('glob');
const {
  app,
  BrowserWindow,Menu
} = require('electron');
const autoUpdater = require('./auto-updater');

const debug = /--debug/.test(process.argv[2]);

if (process.mas) app.setName('Electron APIs');

let mainWindow = null;

const gotTheLock = app.requestSingleInstanceLock()

  if (!gotTheLock) {
    app.quit()
  } else {
    app.on('second-instance', (event, commandLine, workingDirectory) => {
      // Someone tried to run a second instance, we should focus our window.
      if (myWindow) {
        if (myWindow.isMinimized()) myWindow.restore()
        myWindow.focus();
        
      }
    })
  
    // Create myWindow, load the rest of the app, etc...
    // app.on('ready', () => {
     
    // });
  }

function initialize() {

  // const shouldQuit = makeSingleInstance();
  // if (shouldQuit) return app.quit();
 
  loadDemos();

  function createWindow() {
    const windowOptions = {
      titleBarStyle: 'hidden',
      resizable: false,
      frame: false,
      center: true,
      width: 1270,
      height: 725,
      minHeight: 725,
      minWidth: 1270,
      backgroundColor: '#40502f',
      title: app.getName(),
      icon:  __dirname +'/build/1025.png',
      transparent:true
    };

    if (process.platform === 'linux') {
      windowOptions.icon = path.join(__dirname, '/build/1025.png')
    }
    mainWindow = new BrowserWindow(windowOptions);
    mainWindow.loadURL(path.join('file://', __dirname, '/login.html'));
    // let top = new BrowserWindow()
    // let child = new BrowserWindow({ parent: top })
    // child.show()
    // top.show()

    // mainWindow.setAlwaysOnTop(true, 'screen'); //Always on top of all the application
    
    
    // dialog.showMessageBox(
    //   new BrowserWindow({
    //     show: false,
    //     alwaysOnTop: true
    //   }),
    //   {
    //     type: 'question',
    //     message: 'is on top'
    //   }
    // )


    //Launch fullscreen with DevTools open, usage: npm run debug
    //mainWindow.webContents.openDevTools();

    if (debug) {
    //mainWindow.webContents.openDevTools();
    mainWindow.maximize();
      require('devtron').install()
    }

    mainWindow.on('closed', () => {
      mainWindow = null
    });

    const template = [
      
      {
        label: 'View',
        submenu: [
          {role: 'reload'},
          {role: 'forcereload'},
          {role: 'toggledevtools'},
          //{type: 'separator'},
          //{role: 'resetzoom'},
         // {role: 'zoomin'},
          //{role: 'zoomout'},
          //{type: 'separator'},
          //{role: 'togglefullscreen'}
        ]
      },
      {
        role: 'window',
        submenu: [
          {role: 'minimize'},
          //{role: 'close'}
        ]
      },
      {
        role: 'help',
        submenu: [
          {
            label: 'Learn More',
            click () { require('electron').shell.openExternal('https://electronjs.org') }
          }
        ]
      }
    ]
    
    if (process.platform === 'darwin') {
      template.unshift({
        label: app.getName(),
        submenu: [
          {role: 'about'},
          {type: 'separator'},
          {role: 'services', submenu: []},
          {type: 'separator'},
          {role: 'hide'},
          {role: 'hideothers'},
          {role: 'unhide'},
          {type: 'separator'},
          {role: 'quit'}
        ]
      })
    
      // Edit menu
      template[1].submenu.push(
        {type: 'separator'},
        {
          label: 'Speech',
          submenu: [
            {role: 'startspeaking'},
            {role: 'stopspeaking'}
          ]
        }
      )
    
      // Window menu
      template[3].submenu = [
        {role: 'close'},
        {role: 'minimize'},
        {role: 'zoom'},
        {type: 'separator'},
        {role: 'front'}
      ]
    }
    
    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)

  }

  

  app.on('ready', () => {
    createWindow();
    autoUpdater.initialize()
  });

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  });

  app.on('activate', () => {
    if (mainWindow === null) {
      createWindow()
    }
  })
}

// Make this app a single instance app.

// The main window will be restored and focused instead of a second window
// opened when a person attempts to launch a second instance.

// Returns true if the current version of the app should quit instead of
// launching.
// function makeSingleInstance() {
//   if (process.mas) return false;

//   return app.makeSingleInstance(() => {
//     if (mainWindow) {
//       if (mainWindow.isMinimized()) mainWindow.restore();
//       mainWindow.focus()
//     }
//   })
// }



// Require each JS file in the main-process dir
function loadDemos() {
  const files = glob.sync(path.join(__dirname, 'main-process/**/*.js'));
  files.forEach((file) => {
    require(file)
  });
  autoUpdater.updateMenu()
}

// Handle Squirrel on Windows startup events
switch (process.argv[1]) {
  case '--squirrel-install':
    autoUpdater.createShortcut(() => {
      app.quit()
    });
    break;
  case '--squirrel-uninstall':
    autoUpdater.removeShortcut(() => {
      app.quit()
    });
    break;
  case '--squirrel-obsolete':
  case '--squirrel-updated':
    app.quit();
    break;
  default:
    initialize()
}