$(document).on('click', '#eco_all_products', function () {
    $("#main-content").load("eco-products-all.html");
    $('.active').removeClass('active');
    $('#eco_all_products').children(':first').addClass('active');
});
$(document).on('click', '#eco_add_products', function () {
    $("#main-content").load("eco-add-product.html");
    $('.active').removeClass('active');
    $('#eco_add_products').children(':first').addClass('active');
});
$(document).on('click', '#eco_approve_products', function () {
    $("#main-content").load("eco-product-approve.html"); 
    $('.active').removeClass('active');
    $('#eco_approve_products').children(':first').addClass('active');
});
$(document).on('click', '#eco_edit_products', function () {
    $("#main-content").load("eco-edit-product.html"); 
    $('.active').removeClass('active');
    $('#eco_edit_products').children(':first').addClass('active');
});

$(document).on('click', '#eco_products_allot', function () {
    $("#main-content").load("eco-product-allot.html"); 
    $('.active').removeClass('active');
    $('#eco_products_allot').children(':first').addClass('active');
});
$(document).on('click', '#eco-offers-add', function () {
    $("#main-content").load("eco-offers-add.html");
    $('.active').removeClass('active');
    $('#eco-offers-add').children(':first').addClass('active');
});
$(document).on('click', '#eco-offers-add-vendor', function () {
    $("#main-content").load("eco-offers-add-vendor.html");
    $('.active').removeClass('active');
    $('#eco-offers-add-vendor').children(':first').addClass('active');
});
$(document).on('click', '#eco-offers-add-marketplace', function () {
    $("#main-content").load("eco-offers-add-marketplace.html");
    $('.active').removeClass('active');
    $('#eco-offers-add-marketplace').children(':first').addClass('active');
});
$(document).on('click', '#eco-invoices', function () {
    $("#main-content").load("eco-invoices.html"); 
    $('.active').removeClass('active');
    $('#eco-invoices').children(':first').addClass('active');
});
$(document).on('click', '#eco-invoice-add', function () {
    $("#main-content").load("eco-invoice-add.html"); 
    $('.active').removeClass('active');
    $('#eco-invoice-add').children(':first').addClass('active');
});
$(document).on('click', '#eco-invoice-edit', function () {
    $("#main-content").load("eco-invoice-edit.html"); 
    $('.active').removeClass('active');
    $('#eco-invoice-edit').children(':first').addClass('active');
});
$(document).on('click', '#eco-invoice-view', function () {
    $("#main-content").load("eco-invoice-view.html"); 
    $('.active').removeClass('active');
    $('#eco-invoice-view').children(':first').addClass('active');
});
$(document).on('click', '#eco-customers', function () {
    $("#main-content").load("eco-customers.html"); 
    $('#eco-customers').children(':first').addClass('active');
});
$(document).on('click', '#eco-subscribers', function () {
    $("#main-content").load("eco-subscriber.html"); 
    $('#eco-subscribers').children(':first').addClass('active');
});
$(document).on('click', '#eco-customer-add', function () {
    $("#main-content").load("eco-customer-add.html"); 
    $('.active').removeClass('active');
    $('#eco-customer-add').children(':first').addClass('active');
});
$(document).on('click', '#eco-customer-edit', function () {
    $("#main-content").load("eco-customer-edit.html"); 
    $('.active').removeClass('active');
    $('#eco-customer-edit').children(':first').addClass('active');
});
$(document).on('click', '#eco-vendors', function () {
    $("#main-content").load("eco-vendors.html"); 
    $('.active').removeClass('active');
    $('#eco-vendors').children(':first').addClass('active');
});
$(document).on('click', '#eco-vendor-add', function () {
    $("#main-content").load("eco-vendor-add.html"); 
    $('.active').removeClass('active');
    $('#eco-vendor-add').children(':first').addClass('active');
});
$(document).on('click', '#eco-vendor-edit', function () {
    $("#main-content").load("eco-vendor-edit.html"); 
    $('.active').removeClass('active');
    $('#eco-vendor-edit').children(':first').addClass('active');
});
$(document).on('click', '#eco-orders', function () {
    $("#main-content").load("eco-orders.html"); 
    $('.active').removeClass('active');
    $('#eco-orders').children(':first').addClass('active');
});
$(document).on('click', '#eco-order-add', function () {
    $("#main-content").load("eco-order-add.html"); 
    $('.active').removeClass('active');
    $('#eco-order-add').children(':first').addClass('active');
});
$(document).on('click', '#eco-order-edit', function () {
    $("#main-content").load("eco-order-edit.html"); 
    $('.active').removeClass('active');
    $('#eco-order-edit').children(':first').addClass('active');
});
$(document).on('click', '#eco-users', function () {
    $("#main-content").load("eco-users.html"); 
    $('.active').removeClass('active');
    $('#eco-users').children(':first').addClass('active');
});
$(document).on('click', '#eco-user-add', function () {
    $("#main-content").load("eco-user-add.html"); 
    $('.active').removeClass('active');
    $('#eco-user-add').children(':first').addClass('active');
});
$(document).on('click', '#eco-user-edit', function () {
    $("#main-content").load("eco-user-edit.html"); 
    $('.active').removeClass('active');
    $('#eco-user-edit').children(':first').addClass('active');
});
$(document).on('click', '#eco-mail-inbox', function () {
    $("#main-content").load("eco-mail-inbox.html"); 
    $('.active').removeClass('active');
    $('#eco-mail-inbox').children(':first').addClass('active');
    $(".page-topbar").addClass("sidebar_shift").removeClass("chat_shift");
            $(".page-sidebar").addClass("collapseit").removeClass("expandit");
            $("#main-content").addClass("sidebar_shift").removeClass("chat_shift");
            $(".page-chatapi").removeClass("showit").addClass("hideit");
            $(".chatapi-windows").removeClass("showit").addClass("hideit");
});
$(document).on('click', '#eco-mail-compose', function () {
    $("#mail_content").load("eco-mail-compose.html"); 
    $('.activa').removeClass('active');
    $('#eco-mail-compose').addClass('active');
});

$(document).on('click', '#eco-mail-sent', function () {
    $("#mail_content").load("eco-mail-sent.html"); 
    $('.activa').removeClass('active');
    $('#eco-mail-sent').addClass('active');
});
$(document).on('click', '#eco-mail-drafts', function () {
    $("#mail_content").load("eco-mail-drafts.html");
    $('.activa').removeClass('active');
    $('#eco-mail-drafts').addClass('active');
});
$(document).on('click', '#eco-mail-important', function () {
    $("#mail_content").load("eco-mail-important.html"); 
    $('.activa').removeClass('active');
    $('#eco-mail-important').addClass('active');
});
$(document).on('click', '#eco-mail-trash', function () {
    $("#mail_content").load("eco-mail-trash.html");
    $('.activa').removeClass('active');
    $('#eco-mail-trash').addClass('active');
});
$(document).on('click', '#eco-slider-add', function () {
    $("#main-content").load("eco-slider-add.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-add').addClass('active');
});
$(document).on('click', '#eco-slider-franchise-add', function () {
    $("#main-content").load("eco-slider-franchise-add.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-franchise-add').addClass('active');
});
$(document).on('click', '#eco-slider-brand-add', function () {
    $("#main-content").load("eco-slider-brand-add.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-brand-add').addClass('active');
});
$(document).on('click', '#eco-slider-badge-add', function () {
    $("#main-content").load("eco-slider-badge-add.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-badge-add').addClass('active');
});
$(document).on('click', '#eco-slider-edit', function () {
    $("#main-content").load("eco-slider-edit.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-edit').addClass('active');
});

$(document).on('click', '#eco-slider-icecream-type', function () {
    $("#main-content").load("eco-slider-icecream-type.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-icecream-type').addClass('active');
});

$(document).on('click', '#eco-slider-prdtsumary-div', function () {
    $("#main-content").load("eco-slider-prdtsumary-div.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-prdtsumary-div').addClass('active');
});
$(document).on('click', '#eco-slider-static-structure', function () {
    $("#main-content").load("eco-slider-static-structure.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-static-structure').addClass('active');
});
$(document).on('click', '#eco-slider-color', function () {
    $("#main-content").load("eco-slider-color.html"); 
    $('.active').removeClass('active');
    $('#eco-slider-color').addClass('active');
});
$(document).on('click', '#eco-coupons', function () {
    $("#main-content").load("eco-coupons.html");
    $('.active').removeClass('active');
    $('#eco-coupons').addClass('active');
});
$(document).on('click', '#eco-coupons-add', function () {
    $("#main-content").load("eco-coupons-add.html"); 
    $('.active').removeClass('active');
    $('#eco-coupons-add').addClass('active');
});
$(document).on('click', '#eco-coupons-edit', function () {
    $("#main-content").load("eco-coupons-edit.html");
    $('.active').removeClass('active');
    $('#eco-coupons-edit').addClass('active');
});
$(document).on('click','#eco-coupons-barrier',function () {
    $('#main-content').load('eco-coupons-barrier.html');
    $('.active').removeClass('active');
    $('#eco-coupons-barrier').addClass('active');
});
$(document).on('click','#eco-redeem-barrier',function () {
    $('#main-content').load('eco-redeem-barrier.html');
    $('.active').removeClass('active');
    $('#eco-redeem-barrier').addClass('active');
});
$(document).on('click','#eco-redeem-points',function () {
    $('#main-content').load('eco-redeem-points.html');
    $('.active').removeClass('active');
    $('#eco-redeem-points').addClass('active');
});
$(document).on('click','#eco-brands',function () {
    $('#main-content').load('eco-brands.html');
    $('.active').removeClass('active');
    $('#eco-brands').addClass('active');
});
$(document).on('click','#eco-brands-add',function () {
    $('#main-content').load('eco-brands-add.html');
    $('.active').removeClass('active');
    $('#eco-brands-add').addClass('active');
});
$(document).on('click','#eco-category-add',function () {
    $('#main-content').load('eco-category-add.html');
    $('.active').removeClass('active');
    $('#eco-category').addClass('active');
});
$(document).on('click','#eco-sub-category-add',function () {
    $('#main-content').load('eco-sub-category-add.html');
    $('.active').removeClass('active');
    $('#eco-sub-category-add').addClass('active');
});
$(document).on('click','#eco-product-add-hot',function () {
    $('#main-content').load('eco-product-add-hot.html');
    $('.active').removeClass('active');
    $('#eco-product-add-hot').addClass('active');
});
$(document).on('click', "#admin_profile", function () {
    $("#main-content").load("eco-admin-profile.html");
    $('.active').removeClass('active');
})
$(document).on('click', '#eco-account-settings', function () {
    $("#main-content").load("eco-account-settings.html");
    $('.active').removeClass('active');
    $('#eco-account-settings').children(':first').addClass('active');
});
$(document).on('click','#eco-policy-add',function () {
    $('#main-content').load('eco-policy-add.html');
    $('.active').removeClass('active');
    $('#eco-policy-add').addClass('active');
});
$(document).on('click','#eco-events-add',function () {
    $('#main-content').load('eco-events-add.html');
    $('.active').removeClass('active');
    $('#eco-events-add').addClass('active');
});

$(document).on('keyup','.oneSpace',function (e) {
    let sanitized = $(this).val().replace(/\s\s+/g, ' ');
    $(this).val(sanitized);
})

$(document).on('keyup','.alpha_only',function () {   
    let sanitized = $(this).val().replace(/[^a-zA-Z]/g,'');
    $(this).val(sanitized);
});

$(document).on('keyup',".num_only",function () {
    
    let sanitized = $(this).val().replace(/[^0-9]/g,'');
    $(this).val(sanitized);

});

$(document).on('keyup','.num_alpha_only',function () {
    
    let sanitized = $(this).val().replace(/[^a-zA-Z\^0-9\ ']/g,'');
    $(this).val(sanitized);

});

$(document).on('changeDate','#prdt_mfd',function (e) {
    e.preventDefault();
});

$(document).on('changeDate','#prdt_expd',function (e) {
    e.preventDefault();
    if($("#prdt_mfd").val()==''){
        alert('Select Manufacturing Date First');
        $("#prdt_expd").val('');
        return false;
    }
});

function checkAdmin(){

    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    // console.log(decodedCookie)
    //  console.log(ca);
    if(ca[0]==""){
        document.location.href="index.html";
    }
    else{
        $.ajax({
            url:"https://patashalafinder.in/amrutha/getallusers",
            type:"POST",
            data:{
                vendorId:localStorage.getItem('vendor_id')
            },
            success: function (result) {
                let userObj=result.data;
                for(let key in userObj){
                   $("#totalCustomers").text(key);
                }
               
            },
            error: function (err) {
                alert(err.responseJSON.message);
            }
        });
        $.ajax({
            url:"https://patashalafinder.in/amrutha/getorders",
            type:"POST",
            data:{
                vendorId:localStorage.getItem('vendor_id'),
                status:"Transaction Successful"
            },
            success: function (result) {
              
                let orderObject = result.data;
                for(let key in orderObject){
                    $("#totalOrders").text(key);
                }
        
               
            },
            error:function(err){
                console.log(err.responseJSON.message)
            }
        });
    }
    
}

// get cookie name
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
   
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

// log out user when session expires
function fetchData(){
    alert('Your session expired, please login again.');
    $('#logout').trigger('click');
}

// set cookie name
function setCookie(cname, cvalue) {
    var d = new Date();
    de = new Date(d.getTime() + (60*60*1000));
    var expires = "expires="+ de.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// set cookie name
function checkCookie() {
    //console.log('hii');
    var username = getCookie("username");
    if (username == 'true') {
        return true;
    } else {
        return false;
    }
}

function delete_cookie(cname) {
    // alert('deleted');
    var d = new Date();
    de = new Date(d.getTime() + (0));
    var expires = "expires="+ de.toUTCString();
    document.cookie =cname + "=;" + expires + ";path=/";
    // alert(document.cookie);
}

$(document).on('keyup','#user_login', function(){
    if($("#user_login").val().length==10){
        $("#user_pass").removeAttr('disabled');
    }else{
        $("#user_pass").attr('disabled','disabled');
    }
});
$(document).on('submit', '#loginformmVendor', function (e) {
    e.preventDefault();
    let user_name=$('#user_login').val();
    let pass=$('#user_pass').val();
    $.ajax({
        url:"https://patashalafinder.in/amrutha/vendorlogin",
        datatype:"JSON",
        type:"POST",
        data:{
            "mobileNo":user_name,
            "password":pass
        },
        success:function(result){
            console.log(result);
            localStorage.setItem('vendor_id',result.data.uuid)
            setCookie('user-loggedin', 'true')
            document.location.href='index-ecommerce.html'
        },
        error:function(err){
            alert("Username or Password is incorrect");
        }
    })
   
});
$(document).on('submit', '#loginformm', function (e) {
    e.preventDefault();
    let user_name=$('#user_login').val();
    if($("#user_login").val().length!=10){
        $("#alertMsg0").text("Enter 10 Digit Number");
        $("#alertMsg0").slideDown();
        $("#alertMsg0").delay(3000).slideUp(500);
        return false;
    }
    if($("#user_pass").val()==""){
        $("#alertMsg1").text("Enter Password");
        $("#alertMsg1").slideDown();
        $("#alertMsg1").delay(3000).slideUp(500);
        return false;
    }
    let pass=$('#user_pass').val();
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/amrutha_admin_login",
        datatype:"JSON",
        type:"POST",
        data:{
            "mobile_or_email":user_name,
            "password":pass
        },
        success:function(result){
            console.log(result.data[0].admin_uuid);
            localStorage.setItem("adminData",JSON.stringify(result.data[0]));
            localStorage.setItem('admin_uuid',result.data[0].admin_uuid)
            setCookie('admin-loggedin', 'true')
            document.location.href='index-ecommerce.html'
        },
        error:function(err){
            $("#alertMsg1").text("Username Or Password Is Incorrect");
            $("#alertMsg1").slideDown();
            $("#alertMsg1").delay(3000).slideUp(500);
            $("#user_login").val('');
            $("#user_pass").val('');
            $("#user_pass").attr('disabled','disabled');
        }
    })
   
});

$(document).on('keyup','#user_login_otp',function () {
    
    if($("#user_login_otp").val().length==10){
        // alert($('#user_login_otp').val())
        $.ajax({
            url:"https://patashalafinder.in/admin_amrutha/admin_forgot_password",
            datatype:"JSON",
            type:"POST",
            data:{
                "mobile_number":$('#user_login_otp').val(),
            },
            success:function(result){
                $("#alertMsg2").text("OTP Sent");
                $("#alertMsg2").slideDown();
                $("#alertMsg2").delay(3000).slideUp(500);
                $("#user_otp").removeAttr('disabled');
                console.log('sent');
               
            },
            error:function(err){
                $("#alertMsg2").text("Incorrect Phone Number");
                $("#alertMsg2").slideDown();
                $("#alertMsg2").delay(3000).slideUp(500);
            }
        })
    }else{
        $("#alertMsg2").text("Enter Full 10 Digit Mobile No");
        $("#alertMsg2").slideDown();
        $("#alertMsg2").delay(3000).slideUp(500);
        // $('.openOnMobileEnter').hide();
    }

});
$(document).on('keyup', '#user_otp', function(e){
    if($("#user_otp").val().length==4){
        $("#form_submit_otp").removeAttr('disabled');
    }
});
$(document).on('submit','#loginformmOtp',function(e){
    e.preventDefault();
    
      $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/admin_verify_otp",
        datatype:"JSON",
        type:"POST",
        data:{
            mobile_number:$("#user_login_otp").val(),
            mobile_otp:$('#user_otp').val()
        },
        success:function(result){
            localStorage.setItem("adminData",JSON.stringify(result.data[0]));
            localStorage.setItem('admin_uuid',result.data.uuid)
            setCookie('admin-loggedin', 'true')
            document.location.href='index-ecommerce.html'
        },
        error:function(err){
            $("#alertMsg3").text("Username Or Otp Is Incorrect");
            $("#alertMsg3").slideDown();
            $("#alertMsg3").delay(3000).slideUp(500);
            $("#user_login_otp").val('');
            $("#user_otp").val('');
            $("#user_otp").attr('disabled', 'disabled');
            $("#form_submit_otp").attr('disabled', 'disabled');
        }
    })

});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $("#adminImage").attr('src',e.target.result);
            $("#profile_img").attr('src',e.target.result)
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).on('change', "#imageUpload",function() {
    readURL(this);
});
$(document).on('submit', '#admin_profile_form', function (e) {
    e.preventDefault();
    let adminData = JSON.parse(localStorage.getItem("adminData"));
    let newFormData = new FormData();
    newFormData.append("admin_id",localStorage.getItem("admin_uuid"));
    newFormData.append("email_id",adminData.email_id);
    newFormData.append("mobile_no",adminData.mobile_no);
    newFormData.append("aadhaar_id", $("#aadharId_admin").val()!='' ? $("#aadharId_admin").val() : null );
    newFormData.append("address", $("#address_admin").val()!='' ? $("#address_admin").val() : null );
    newFormData.append("date_of_birth", $("#dob_admin").val()!='' ? $("#dob_admin").val() : null )
    newFormData.append("description", $("#description_admin").val()!='' ? $("#description_admin").val() : null )
    newFormData.append("facebook_url", $("#facebook_url_admin").val()!='' ? $("#facebook_url_admin").val() : null )
    newFormData.append("twitter_url", $("#twitter_url_admin").val()!='' ? $("#twitter_url_admin").val() : null )
    newFormData.append("linkedin_url", $("#linkedIn_url_admin").val()!='' ? $("#linkedIn_url_admin").val() : null )
    newFormData.append("google_plus_url", $("#googlePlus_url_admin").val()!='' ? $("#googlePlus_url_admin").val() : null )
    newFormData.append("first_name", $("#first_name_admin").val()!='' ? $("#first_name_admin").val() : null )
    newFormData.append("last_name", $("#last_name_admin").val()!='' ? $("#last_name_admin").val() : null )
    newFormData.append("profile_image", document.querySelector('#imageUpload').files[0] );
    newFormData.append("gender",  $('input[name=inlineRadioOption]:checked').val() );
    newFormData.append("updated_date",  moment.utc().format() );

    $.ajax({
        url: "https://patashalafinder.in/admin_amrutha/update_admin_profile",
        type: "POST",
        processData: false,
        contentType: false,
        data: newFormData,
        success: function (result) {
            // alert(result);
            console.log(result);
        },
        error: function (err) {
            console.log(err.responseJSON);
            alert(err.responseJSON);
        }
    });

});
$(document).on('change keyup','.mobNo',function () {
    var sanitized = $(this).val().replace(/[^0-9]/g, '');
    $(this).val(sanitized);
});
$(document).on('submit','#addCategoryForm',function (e) {
    e.preventDefault();

    let categoryStringComaSeperated = $("#tagsinput-2").val();
    let categoryList = categoryStringComaSeperated.split(',');
    let categoryListStr= "";
    let startstr = "['";
    let endStr = "']";
    let middleStr ="','";
    
    if(categoryList == ""){
        alert('Enter Category');
        return false;
    }
    else if(categoryList.length == 1){
        categoryListStr=categoryListStr+startstr+categoryList[0].replace(/^"|"$/g, '')+endStr;
    }else{
        for(let i=0; i<categoryList.length;i++){
            if(i==0){
                categoryListStr=categoryListStr+startstr+categoryList[i].replace(/^"|"$/g, '');
            }else if(i == categoryList.length-1){
                categoryListStr=categoryListStr+middleStr+categoryList[i].replace(/^"|"$/g, '')+endStr;
            }else{
                categoryListStr=categoryListStr+middleStr+categoryList[i].replace(/^"|"$/g, '');
            }
        }
    }
    
   //console.log(categoryListStr);
    
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/add_category",
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem("admin_uuid"),
            category_name_list:categoryListStr
        },
        success:function (result) {
            //console.log(result);
            showAlertToast(result.message);
            document.addCategoryForm.reset();
            $("#eco-category-add").trigger('click')
        },
        error:function (err) {
            showAlertToast(err.responseJSON.message);
        }
    })
});
$(document).on('submit','#addSubCategoryForm', (e) => {
    e.preventDefault();
    let catSelected = $("#cate_add").val();
   
    let subCategoryStringComaSeperated = $("#tagsinput-3").val();
    let subCategoryList = subCategoryStringComaSeperated.split(',');
    let subCategoryListStr= "";
    let startstr = "['";
    let endStr = "']";
    let middleStr ="','";
    if(catSelected == ""){
        showAlertToast("Select Category");
        return false;
    }else if(subCategoryList == ""){
        showAlertToast("Enter Sub Category");
        return false;
    }
    else if(subCategoryList.length == 1){
        subCategoryListStr=subCategoryListStr+startstr+subCategoryList[0].replace(/^"|"$/g, '')+endStr;
    }else{
        for(let i=0; i<subCategoryList.length;i++){
            if(i==0){
                subCategoryListStr=subCategoryListStr+startstr+subCategoryList[i].replace(/^"|"$/g, '');
            }else if(i == subCategoryList.length-1){
                subCategoryListStr=subCategoryListStr+middleStr+subCategoryList[i].replace(/^"|"$/g, '')+endStr;
            }else{
                subCategoryListStr=subCategoryListStr+middleStr+subCategoryList[i].replace(/^"|"$/g, '');
            }
        }
    }
    let finalString= "{"+"'"+catSelected+"'"+':'+subCategoryListStr+"}"
    //console.log(finalString);
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/add_sub_category",
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem("admin_uuid"),
            subcategory_name_list:finalString
        },
        success:function (result) {
            showAlertToast(result.message);
            document.addSubCategoryForm.reset();
            $("#eco-sub-category-add").trigger('click');
            
        },
        error:function (err) {
            showAlertToast(err.responseJSON.message);
        }
    })
});
$(document).on('click','.activate', function () {
    let btnValue = $(this).attr('id').split(" ");
    let catName = btnValue[0];
    let catId = btnValue[1];
    //console.log(btnValue);
    let catSta = "{"+"'1'"+":{"+"'category_id'"+":"+"'"+catId+"'"+","+"'category_status'"+":"+"'True'"+"}}";
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/update_category",
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem("admin_uuid"),
            category_status_list:catSta
        },
        success:function (result) {
            showAlertToast(result.message)
           $("#eco-category-add").trigger('click')
        },
        error:function (err) {
            showAlertToast(err.responseText);
        }
    })
});
$(document).on('click','.inactivate', function () {
    let btnValue = $(this).attr('id').split(" ");
    let catName = btnValue[0];
    let catId = btnValue[1];
    //console.log(btnValue);
    let catStrList = "{"+"'1'"+":{"+"'category_id'"+":"+"'"+catId+"'"+","+"'category_status'"+":"+"'False'"+"}}";
    //console.log(catStrList)
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/update_category",
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem("admin_uuid"),
            category_status_list:catStrList
        },
        success:function (result) {
            showAlertToast(result.message);
            $("#eco-category-add").trigger('click')
        },
        error:function (err) {
            showAlertToast(err.responseText);
        }
    })
});
$(document).on('click','.actDeactSubCat',function () {
    const thisVal = $(this).attr('id').split("-");
    const status = $(this).val();
    const category = thisVal[0].split("_").join(" ");
    const subCat = thisVal[1].split("_").join(" ");
    
    const updateObj = {}
    //{'samosa':{'1':{'subcategory_name':'chicken','subcategory_status':'False'}}}
    const innerObj ={
        'subcategory_name':subCat,
        'subcategory_status': status
    }
    const innerInEachObj = {
        '1':innerObj
    }
    updateObj[category] = innerInEachObj;
    //console.log(JSON.stringify(updateObj));
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/update_sub_category",
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem("admin_uuid"),
            subcategory_updated_list: JSON.stringify(updateObj)
        },
        success:function (result) {
            //console.log(result);
           $("#eco-sub-category-add").trigger('click');
           showAlertToast("Status Updated Successfully.")
        },
        error:function (err) {
            alert(err.responseJSON.message);
        }
    })
})
let web_images_arr=[];
let android_images_arr=[];

let image_id_holder="";

function displayImageArr(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            // console.log(e.target.result);
            $("#"+image_id_holder).attr('src',e.target.result);
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}

$(document).on('change','#trigger_image_upload',function(){
    displayImageArr(this);
    let filename=$("#trigger_image_upload").get(0);
    let thatfile=filename.files;
    // console.log(image_id_holder.split('_')[1])
    if(image_id_holder.split("_")[1]=="1"){
        if(image_id_holder.split("_")[0]=="image1"){
            web_images_arr[0]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image2"){
            web_images_arr[1]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image3"){
            web_images_arr[2]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image4"){
            web_images_arr[3]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image5"){
            web_images_arr[4]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image6"){
            web_images_arr[5]=thatfile[0];
        }
    }else if(image_id_holder.split("_")[1]=="2"){
        if(image_id_holder.split("_")[0]=="image1"){
            android_images_arr[0]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image2"){
            android_images_arr[1]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image3"){
            android_images_arr[2]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image4"){
            android_images_arr[3]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image5"){
            android_images_arr[4]=thatfile[0];
        }else if(image_id_holder.split("_")[0]=="image6"){
            android_images_arr[5]=thatfile[0];
        }
    }
    // console.log(android_images_arr);
    // console.log(web_images_arr);
    // image_id_holder="";
});

$(document).on('click','.trigger-file',function(){
    image_id_holder=$(this).attr('class').split(" ")[1];
    $('#trigger_image_upload').trigger('click');
});

$(document).on('submit',"#addPrdt",function (e) {
    e.preventDefault();
   
    var fileData = new FormData();
    fileData.append("name",$("#prdt_name").val());
    fileData.append("vendorId",localStorage.getItem('vendor_id'));
    fileData.append("category",$("#prdt_cat").val());
    fileData.append("subcategory",$("#prdt_sub_cat").val());
    fileData.append("weight",$("#prdt_weight").val());
    fileData.append("price",$("#prdt_price").val());
    fileData.append("batch_quantity",$("#prdt_quantity").val());
    fileData.append("batchId",$("#prdt_batch_id").val());
    fileData.append("manufacturing_date",""+$("#prdt_mfd").val()+" "+"20:20");
    fileData.append("expiry_date",""+$("#prdt_expd").val()+' '+'20:20');
    fileData.append("description",$("#prdt_desc").val());
    fileData.append("howItMade",$("#prdt_desc_madehow").val());
    fileData.append("ingredients",$("#prdt_desc_ingredients").val());
    fileData.append("disclaimer",$("#prdt_desc_sellermsg").val());
    fileData.append("shortDescription",$('#prdt_short_desc').val());
    fileData.append("unit",$("#prdt_unit").val());
    fileData.append("image1_1",web_images_arr[0]);
    fileData.append("image2_1",web_images_arr[1]);
    fileData.append("image3_1",web_images_arr[2]);
    fileData.append("image4_1",web_images_arr[3]);
    fileData.append("image5_1",web_images_arr[4]);
    fileData.append("image6_1",web_images_arr[5]);
    fileData.append("image1_2",android_images_arr[0]);
    fileData.append("image2_2",android_images_arr[1]);
    fileData.append("image3_2",android_images_arr[2]);
    fileData.append("image4_2",android_images_arr[3]);
    fileData.append("image5_2",android_images_arr[4]);
    fileData.append("image6_2",android_images_arr[5]);
    // console.log(addPrdtObj);
    // console.log(JSON.stringify(add_prdt_obj));
    $.ajax({
        url: "https://patashalafinder.in/amrutha/addproduct",
        type: "POST",
        processData: false,
        contentType: false,
        data: fileData,
        success: function (result) {
            // alert(result);
            console.log(result);
        },
        error: function (err) {
            console.log(err.responseJSON);
            alert(err.responseJSON);
        }
    });
});

$(document).on('change','#prdt_img',function(){
    alert('hi');
    let prdt_images = $("#prdt_img").get(0);
    let eachImg = prdt_images.files;
    // console.log(eachImg)
    if(eachImg.length<3){
        alert("Add Atleast 3 Images");
        $("#prdt_img").val('');
    }
});

$(document).on('click','#logout',function () {
    delete_cookie('admin-loggedin');
    localStorage.setItem('admin_uuid',null);
    localStorage.setItem('adminData',null);

    document.location.href='login.html';
   
});

$(document).on("change","#prdt_ofer_type",function () {

    if($('#franchiseSel').val() == null){
        showAlertToast('Select Franchise');
        $(this).val("Select One");
        return false;
    }
    if($('#prdt_id_ofer').val() == null){
        showAlertToast('Select Product');
        $(this).val("Select One");
        return false;
    }
    if($("#prdt_ofer_type").val() == "Bulk Offer"){
        document.getElementById('tabContainerItem').style.display="none";
        document.getElementById('tabContainerBulk').style.display="flex";
        document.getElementById('itemDiv').style.display="none";
        document.getElementById('bulkDiv').style.display="block";
    }else if($("#prdt_ofer_type").val() == "Item Offer"){
        document.getElementById('tabContainerBulk').style.display="none";
        document.getElementById('tabContainerItem').style.display="flex";
        document.getElementById('itemDiv').style.display="block";
        document.getElementById('bulkDiv').style.display="none";
    }
});

$(document).on('change','#prdt_cat_ofer',function (){

        // $.ajax({
        //     url: "https://patashalafinder.in/amrutha/getproducts",
        //     type: "POST",
        //     data: {
        //         "vendorId":localStorage.getItem('vendor_id'),
        //         "category":$("#prdt_cat_ofer").val(),
        //     },
        //     success: function (result) {
        //         console.log(result);
        //         $("#prdt_id_ofer").empty();
        //         let objFull=result.data.Products;
        //         $("#prdt_id_ofer").append('<option >Select</option>');
        //         for (let key in objFull) {
        //             let objNext=objFull[key];
        //             if(objNext){
        //                 for(let keye in objNext){
        //                     $("#prdt_id_ofer").append('<option value="'+''+objNext[keye]+'-'+key+'">'+objNext[keye]+'</option>');
        //                 }
        //             }
                
        //         }
                
        //     },
        //     error: function (err) {
        //         alert(err.statusText);
        //     }
        // })

});

$(document).on('change','#prdt_id_ofer',function (){

    // if($("#prdt_cat_ofer").val()){
    //     $.ajax({
    //         url: "https://patashalafinder.in/amrutha/getbatch",
    //         type: "POST",
    //         data: {
    //             name:$("#prdt_id_ofer").val().split("-")[0],
    //             vendorId:localStorage.getItem('vendor_id'),
    //             status:'onStock',
    //         },
    //         success: function (result) {
    //             $("#prdt_batch_id_ofer").empty();
    //             let objFull=result.data;
    //                 for(let key in objFull){
                       
    //                     let eachBatch = objFull[key][0];
    //                        let eachBatchUtc = moment().utc(eachBatch["expiry_date"]).format();
    //                        let eachBatchLocal = moment().utc(eachBatchUtc).format('YYYY-MM-DD HH:mm:ss');
    //                     $("#prdt_batch_id_ofer").append('<option value="'+eachBatch["batchUUID"]+'">'+''+eachBatch["batchUUID"]+' '+eachBatchLocal+'</option>');

    //                 }
    //         },
    //         error: function (err) {
    //             alert(err.statusText);
    //         }
    //     })
    // }else{
    //             // $("#alert_for_cat").text("Select Category");
    //             $("#alert_for_cat").show();
    //             $("#alert_for_cat").delay(2000).fadeOut(500);
        
    // }
    
});

$(document).on('change','#prdt_batch_id_ofer',function () {
    if($("#alert_for_prdt_name").val() == ""){
            $("#alert_for_prdt_name").show();
            $("#alert_for_prdt_name").delay(2000).fadeOut(500);
        return false;
    }
})
$(document).on('change','#prdt_cat_edit',function () {
    $.ajax({
        url: "https://patashalafinder.in/amrutha/getproducts",
        type: "POST",
        data: {
            "vendorId":localStorage.getItem('vendor_id'),
            "category":$("#prdt_cat_edit").val(),
        },
        success: function (result) {
            $("#prdt_sub_cat_edit").empty();
            let objFull=result.data.Products;
            $("#prdt_sub_cat_edit").append('<option >Select</option>');
            for (let key in objFull) {
                let objNext=objFull[key];
                if(objNext){
                    for(let keye in objNext){
                        $("#prdt_sub_cat_edit").append('<option value="'+''+objNext[keye]+'-'+key+'">'+objNext[keye]+'</option>');
                    }
                }
            
            }
            
        },
        error: function (err) {
            alert(err.statusText);
        }
    })
});
$(document).on('change','#prdt_sub_cat_edit',function () {
    if($("#prdt_sub_cat_edit").val()){
        $.ajax({
            url: "https://patashalafinder.in/amrutha/getbatch",
            type: "POST",
            data: {
                name:$("#prdt_sub_cat_edit").val().split("-")[0],
                vendorId:localStorage.getItem('vendor_id'),
                status:'onStock',
            },
            success: function (result) {
                $("#prdt_batch_id_edit").empty();
                let objFull=result.data;
                console.log(objFull)
                    for(let key in objFull){
                        let eachBatch = objFull[key][0];
                        let eachBatchUtc = moment().utc(eachBatch["expiry_date"]).format();
                        let eachBatchLocal = moment().utc(eachBatchUtc).format('YYYY-MM-DD HH:mm:ss')
                        $("#prdt_batch_id_edit").append('<option value="'+eachBatch["batchUUID"]+'">'+''+eachBatch["batchUUID"]+' '+eachBatchLocal+'</option>');
                        $("#image1_1-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image1_1']}`);
                        $("#image2_1-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image2_1']}`)
                        $("#image3_1-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image3_1']}`)
                        $("#image4_1-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image4_1']}`)
                        $("#image5_1-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image5_1']}`)
                        $("#image6_1-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image6_1']}`)
                        $("#image1_2-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image1_2']}`)
                        $("#image2_2-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image2_2']}`)
                        $("#image3_2-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image3_2']}`)
                        $("#image4_2-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image4_2']}`)
                        $("#image5_2-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image5_2']}`)
                        $("#image6_2-edit").attr('src','https://patashalafinder.in/media/'+`${eachBatch['image6_2']}`)

                    }
            },
            error: function (err) {
                alert(err.statusText);
            }
        })
    }
});
$(document).on('change','#prdt_cat_hot',function () {
    $.ajax({
        url: "https://patashalafinder.in/amrutha/getproducts",
        type: "POST",
        data: {
            "vendorId":localStorage.getItem('vendor_id'),
            "category":$("#prdt_cat_hot").val(),
        },
        success: function (result) {
            $("#prdt_sub_cat_hot").empty();
            let objFull=result.data.Products;
            $("#prdt_sub_cat_hot").append('<option >Select</option>');
            for (let key in objFull) {
                let objNext=objFull[key];
                if(objNext){
                    for(let keye in objNext){
                        $("#prdt_sub_cat_hot").append('<option value="'+''+objNext[keye]+'-'+key+'">'+objNext[keye]+'</option>');
                    }
                }
            
            }
            
        },
        error: function (err) {
            alert(err.statusText);
        }
    })
});

let images_edit_arr=[];


let image_edit_id_holder="";

function editDisplayImageArr(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            // console.log(e.target.result);
            $("#"+image_edit_id_holder).attr('src',e.target.result);
            let newImgData = new FormData();
            newImgData.append("batch_uuid",$("#prdt_batch_id_edit").val());
            newImgData.append("image_keyname",image_edit_id_holder.split("-")[0]);
            newImgData.append("updated_image",images_edit_arr[0]);
            $.ajax({
                url:"https://patashalafinder.in/amrutha/update_batch_images",
                type:"POST",
                processData: false,
                contentType: false,
                data:newImgData,
                success:function (result) {
                    // console.log(result);
                    images_edit_arr=[];
                    image_edit_id_holder="";
                },
                error:function (err) {
                    alert(err.responseJSON.message);
                }
            })
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}
$(document).on('change','#trigger_image_upload_edit',function(){
    editDisplayImageArr(this);
    let filename=$("#trigger_image_upload_edit").get(0);
    let thatfile=filename.files;
    images_edit_arr.push(thatfile[0]);
   
  
});

$(document).on('click','.trigger-file-edit',function(){
    
    if($("#prdt_cat_edit").val() == null || $("#prdt_sub_cat_edit").val() == null || $("#prdt_batch_id_edit").val() == null){
        showAlertToast('Select All The Fields');
        return false;
    }
    image_edit_id_holder=$(this).attr('class').split(" ")[1];
    $('#trigger_image_upload_edit').trigger('click');
});


$(document).on('change','#prdt_id_ofer',function () {
    if($('#franchiseSel').val() == null){
        showAlertToast('Select Franchise');
        $(this).val("Select One");
        return false;
    }
    
});
$(document).on('click','#add_fixed_item_discount',function (e) {
    
    if($('#offer_start_date').val() == ''){
        showAlertToast('Set Offer Activate Date');
       
        return false;
    }
    if($('#offer_end_date').val() == ''){
        showAlertToast('Set Offer Expiry Date');
       
        return false;
    }
    let formData = new FormData();
        formData.append('admin_uuid',localStorage.getItem('admin_uuid'));
        formData.append('franchise_uuid',$('#franchiseSel').val());
        formData.append('activate_date',$('#offer_start_date').val())
        formData.append('expire_date',$('#offer_end_date').val())
        formData.append('productId',$('#prdt_id_ofer').val());
        formData.append('offer_sub_type',"FIXED");
        formData.append('offer_amount',$('#fixed_item_discount').val());
        // for(let key of formData.entries()){
        //     console.log(key[0]+':'+key[1])
        // }
   
    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/add_item_offer",
        "method": "POST",
        "data": formData,
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        success: function (result) {
            // console.log(result.status_code);
            console.log(result);
          
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });
});
$(document).on('click','.addFieldButtonBulkUnit',function () {
    //console.log($(this).val());
    const currentVal =Number($(this).val().split(" ")[0])+1;
    const removeOrAdd = $(this).val().split(" ")[1]
    // $(this).attr('disabled','disabled');
    $(this).val(""+$(this).val().split(" ")[0]+" "+"minus")
    $(this).html("x");
    if(removeOrAdd == "plus"){
        $("#appendBulkUnit").append(`
        <div id="addRemove${currentVal}">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    
        <div class="form-group">
            <label class="form-label" for="unit_bulk_buy_offer${currentVal}">Buy Unit</label>
            <span class="desc">e.g. "Quantity ofitem 1,2 etc."</span>
            <div class="controls">
                <input autocomplete="off" type="text" value="" class="form-control num_alpha buyUnitBulk" id="unit_bulk_buy_offer${currentVal}" required>
            </div>
        </div>
        <span class="alertSpan" id="alert_for_unit_bulk_buy_offer${currentVal}">Enter Buy Quantity</span>

    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

        <div class="form-group">
            <label class="form-label" for="unit_bulk_get_offeer${currentVal}">Get Unit</label>
            <span class="desc">e.g. "Quantity free item 1,2 etc."</span>
            <div class="controls">
                <input autocomplete="off" type="text" value="" class="form-control num_alpha getUnitBulk" id="unit_bulk_get_offer${currentVal}" required>
            </div>
        </div>
        <span class="alertSpan" id="alert_for_unit_bulk_get_offer${currentVal}">Enter Get Quantity</span>

    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <div class="text-left" style="margin:10% 0%">
            <button type="button" class="btn btn-primary addFieldButtonBulkUnit" value="${currentVal} plus">+</button>
        </div>
    </div>
        </div>
    `)
    }else if(removeOrAdd == "minus" && $(this).val().split(" ")[0] != "1"){
        $("#addRemove"+$(this).val().split(" ")[0]).remove();
    }
    
});
$(document).on('click','.addFieldButtonBulkPercent',function () {
    //console.log($(this).val());
    const currentVal =Number($(this).val().split(" ")[0])+1;
    const removeOrAdd = $(this).val().split(" ")[1]
    // $(this).attr('disabled','disabled');
    $(this).val(""+$(this).val().split(" ")[0]+" "+"minus")
    $(this).html("x");
    if(removeOrAdd == "plus"){
        $("#appendBulkPercent").append(`
        <div id="addRemovePercent${currentVal}">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    
                                        <div class="form-group">
                                            <label class="form-label" for="unit_bulk_buy_percent_offer${currentVal}">Buy Unit</label>
                                            <span class="desc">e.g. "Quantity of item 1,2 etc.."</span>
                                            <div class="controls">
                                                <input autocomplete="off" type="text" value="" class="form-control num_alpha buyPercentBulk" id="unit_bulk_buy_percent_offer${currentVal}" required>
                                            </div>
                                        </div>
                                        <span class="alertSpan" id="alert_for_unit_bulk_get_offer${currentVal}">Enter Buy Quantity</span>
    
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
    
                                        <div class="form-group">
                                            <label class="form-label" for="unit_bulk_get_percent_offer${currentVal}">Percentage Off</label>
                                            <span class="desc">e.g. "% off on buying item 1,2 etc."</span>
                                            <div class="controls">
                                                <input autocomplete="off" type="text" value="" class="form-control num_alpha getPercentBulk" id="unit_bulk_get_percent_offer${currentVal}" required>
                                            </div>
                                        </div>
                                        <span class="alertSpan" id="alert_for_unit_bulk_get_offer${currentVal}">Enter Percent Offered</span>
    
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="text-left" style="margin: 10% 0%;">
                                            <button type="button" class="btn btn-primary addFieldButtonBulkPercent" value="${currentVal} plus">+</button>
                                        </div>
                                    </div>
        </div>
    `)
    }else if(removeOrAdd == "minus" && $(this).val().split(" ")[0] != "1"){
        $("#addRemovePercent"+$(this).val().split(" ")[0]).remove();
    }
    
})
$(document).on('click','#add_fixed_item_discount_percent',function (e) {

    if($('#offer_start_date').val() == ''){
        showAlertToast('Set Offer Activate Date');
       
        return false;
    }
    if($('#offer_end_date').val() == ''){
        showAlertToast('Set Offer Expiry Date');
       
        return false;
    }
    let formData = new FormData();
    formData.append('admin_uuid',localStorage.getItem('admin_uuid'));
    formData.append('franchise_uuid',$('#franchiseSel').val());
    formData.append('activate_date',$('#offer_start_date').val())
    formData.append('expire_date',$('#offer_end_date').val())
    formData.append('productId',$('#prdt_id_ofer').val());
    formData.append('offer_sub_type',"PERCENT");
    formData.append('offer_percent',$('#fixed_item_discount_percent').val());
    for(let key of formData.entries()){
        console.log(key[0]+':'+key[1])
    }

    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/add_item_offer",
        "method": "POST",
        "data": formData,
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        success: function (result) {          
            console.log(result);           
        },
        error: function (err) {
            console.log(err.responseText);
        }
    });
});

$(document).on('click','#add_unit_bulk_buy_get_offer',function (e) {

    if($('#franchiseSel').val() == null){
        showAlertToast('Select Franchise');
        $(this).val("Select One");
        return false;
    }
    if($('#offer_start_date').val() == ''){
        showAlertToast('Set Offer Activate Date');
       
        return false;
    }
    if($('#offer_end_date').val() == ''){
        showAlertToast('Set Offer Expiry Date');
       
        return false;
    }
   let bigObj={

   }
   
    $(".buyUnitBulk").each(function(key,element){
        //console.log(key,$(this).val());
        if($(this).val() ==""){
            showAlertToast('Enter All the Fields');
            bigObj={

            }
        return false;
        }
        bigObj[key+1] = [$(this).val()]
    });
    $(".getUnitBulk").each(function(key,element){
        //console.log(key,$(this).val());
        if($(this).val() ==""){
            showAlertToast('Enter All the Fields');
            bigObj={

            }
        return false;
        }
        bigObj[key+1].push($(this).val());
    })
    //console.log(JSON.stringify(bigObj))
    //return false;

    let formData = new FormData();
        formData.append('admin_uuid',localStorage.getItem('admin_uuid'));
        formData.append('franchise_uuid',$('#franchiseSel').val());
        formData.append('activate_date',$('#offer_start_date').val())
        formData.append('expire_date',$('#offer_end_date').val())
        formData.append('productId',$('#prdt_id_ofer').val());
        formData.append('offer_sub_type',"UNIT");
        formData.append('bulk_offer_list',JSON.stringify(bigObj));
        // for(let key of formData.entries()){
        //     console.log(key[0]+':'+key[1])
        // }
   
    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/add_bulk_offer",
        "method": "POST",
        "data": formData,
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        success: function (result) {
            // console.log(result.status_code);
            console.log(result);
            showAlertToast('Offer Added Successfully');
        },
        error: function (err) {
            console.log(err.responseText);
            showAlertToast(err.responseText);
        }
    });
   
    
});

$(document).on('click','#add_unit_bulk_get_percent_offer',function (e) {
    if($('#franchiseSel').val() == null){
        showAlertToast('Select Franchise');
        $(this).val("Select One");
        return false;
    }
    if($('#offer_start_date').val() == ''){
        showAlertToast('Set Offer Activate Date');
       
        return false;
    }
    if($('#offer_end_date').val() == ''){
        showAlertToast('Set Offer Expiry Date');
       
        return false;
    }
   let bigObj={

   }
   $(".buyPercentBulk").each(function(key,element){
    //console.log(key,$(this).val());
        if($(this).val() ==""){
            showAlertToast('Enter All the Fields');
            bigObj={

            }
        return false;
        }
        bigObj[key+1] = [$(this).val()]
    });
    $(".getPercentBulk").each(function(key,element){
        //console.log(key,$(this).val());
        if($(this).val() ==""){
            showAlertToast('Enter All the Fields');
            bigObj={

            }
        return false;
        }
        bigObj[key+1].push($(this).val());
    })
    //console.log(JSON.stringify(bigObj))
    let formData = new FormData();
    formData.append('admin_uuid',localStorage.getItem('admin_uuid'));
    formData.append('franchise_uuid',$('#franchiseSel').val());
    formData.append('activate_date',$('#offer_start_date').val())
    formData.append('expire_date',$('#offer_end_date').val())
    formData.append('productId',$('#prdt_id_ofer').val());
    formData.append('offer_sub_type',"PERCENT");
    formData.append('bulk_offer_list',JSON.stringify(bigObj));
    // for(let key of formData.entries()){
    //     console.log(key[0]+':'+key[1])
    // }

    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/add_bulk_offer",
        "method": "POST",
        "data": formData,
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        success: function (result) {
            // console.log(result.status_code);
            console.log(result);
            showAlertToast('Offer Added Successfully');
        },
        error: function (err) {
            console.log(err.responseText);
            showAlertToast('err.responseText');
        }
    });
    
});

$(document).on('click',"input[name='cartOffer']",function () {
    let thisVal = $("input[name='cartOffer']:checked").val();
    //console.log(thisVal);
    if(thisVal == "amount"){
        $(".showOnAmount").show();
        $(".showOnVolume").hide();
    }else if(thisVal == "volume"){
        $(".showOnVolume").show();
        $(".showOnAmount").hide();
    }
   
});

$(document).on('change','#selFranchiseCart',function () {
    let formData = new FormData();
    formData.append("admin_uuid", localStorage.getItem('admin_uuid'));
    formData.append('franchise_uuid',$(this).val())
    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/get_cart_offer",
        "method": "POST",
        "data": formData,
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        success: function (result) {
            //console.log(result.status_code);
            let resMsg = JSON.parse(result).data;
            //console.log(resMsg);
             $("#appendCartOfferTable").empty()
                $("#appendCartOfferTable").append(`
                <label>Added Offers For This Franchise</label>
                <table id="example-1" class="table table-striped dt-responsive display" cellspacing="0" width="100%" style="font-size:12px">
                <thead>
                                <tr>
                                    <th class="sorting_disabled">Type</th>
                                    <th>Status</th>
                                    <th>Added On</th>
                                    <th>Active From</th>
                                    <th>Expiring On</th>
                                    <th>Min Cart Amount/Volume</th>
                                    <th>Percent/Discount</th>
                                    
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tfoot>
                                <tr>
                                    <th class="sorting_disabled">Type</th>
                                    <th>Status</th>
                                    <th>Added On</th>
                                    <th>Active From</th>
                                    <th>Expiring On</th>
                                    <th>Min Cart Amount/Volume</th>
                                    <th>Percent</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>

                            <tbody id="appendCartOfferTableAdmin">
                            </tbody>
                    </table>
                `);
                $.each(resMsg,(key,value)=>{
                    $("#appendCartOfferTableAdmin").append(`
                            <tr id="`+value.offer_uuid+`">
                                    <td>${value.cart_volume ? "Volume" : "Amount"}</td>
                                    <td>`+value.cartoffer_status+`</td>
                                    <td >${value.added_date}</td>
                                    <td >${value.activate_date.split("T")[0]}</td>
                                    <td>${value.expire_date.split("T")[0]}</td>
                                    <td>${value.cart_volume ? value.cart_volume : value.cart_amount}</td>
                                    <td>${value.cart_volume ? value.cart_offer_amount : value.cart_offer_percentage}</td>
                                   
                                    <td><span class="editCartOffer"><i class="fa fa-edit"></i></span></td>
                            </tr>
                                
                        `);
                })
        },
        error: function (err) {
            let errMsg = JSON.parse(err.responseText);
            console.log(errMsg);
            showAlertToast(errMsg.message);
        }
});
})
$(document).on('click','#add_cart_offer',function () {
    if($('#selFranchiseCart').val() == null){
        showAlertToast('Select Franchise');
        $(this).val("Select One");
        return false;
    }
    if($('#cart_offer_start_date').val() == ''){
        showAlertToast('Set Offer Activate Date');
       
        return false;
    }
    if($('#cart_offer_end_date').val() == ''){
        showAlertToast('Set Offer Expiry Date');
       
        return false;
    }
    let formData = new FormData();
    formData.append('admin_uuid',localStorage.getItem('admin_uuid'));
    formData.append('franchise_uuid',$('#selFranchiseCart').val());
    formData.append('activate_date',$('#cart_offer_start_date').val())
    formData.append('expire_date',$('#cart_offer_end_date').val());

    let thisVal = $("input[name='cartOffer']:checked").val();
    //console.log(thisVal);
    if(thisVal == "amount"){
        if($('#min_cart_val_offer').val() == ''){
            showAlertToast('Set minimum cart amount.');
           
            return false;
        }
        if($('#percent_offer_on_min_cart').val() == ''){
            showAlertToast('Set offer percent.');
           
            return false;
        }
        formData.append('cart_amount',$('#min_cart_val_offer').val());
        formData.append('cart_offer_percentage',$('#percent_offer_on_min_cart').val());
    }else if(thisVal == "volume"){
        if($('#min_cart_vol_offer').val() == ''){
            showAlertToast('Set minimum cart volume.');
           
            return false;
        }
        if($('#offer_amount_on_cart_volume').val() == ''){
            showAlertToast('Set offer amount.');
           
            return false;
        }
        formData.append('cart_volume',$('#min_cart_vol_offer').val());
        formData.append('cart_offer_amount',$('#offer_amount_on_cart_volume').val());
    }
    
   

    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/add_cart_offer",
        "method": "POST",
        "data": formData,
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        success: function (result) {
           let resMsg = JSON.parse(result);
           console.log(resMsg)
            showAlertToast(resMsg.message);
        },
        error: function (err) {
           
            let errMsg = JSON.parse(err.responseText);
            console.log(errMsg);
            showAlertToast(errMsg.message);
        }
    });
});

$(document).on('submit','#addCoupons',function (e) {
    e.preventDefault();
    var form = new FormData();
    form.append("admin_uuid", localStorage.getItem('admin_uuid'));
    form.append("coupon_type", $("#coupon_type").val());
    form.append("coupon_code", $("#coupon_code").val());
    
    form.append("minimum_amount",$("#cart_value_to_avail_coupon").val());
    form.append("maximum_discount_amount", $("#coupon_amount_max_get").val());
    form.append("usage_times",$("#coupon_usage_times").val());
    form.append("activate_date",$("#coupon_start_date").val());
    form.append("expire_date", $("#coupon_end_date").val());
    form.append("terms",$("#terms").val());
    if($("#coupon_type").val() == "Signup" || $("#coupon_type").val() == "Fixed"){
    
        form.append("coupon_amount", $("#coupon_amount_each").val());
        
    }else{
        form.append("coupon_percent", $("#coupon_percent").val());
        
    }
    
   
    
    $.ajax({
      "async": true,
      "crossDomain": true,
      "url": "https://patashalafinder.in/admin_amrutha/add_coupons",
      "method": "POST",
      
      "processData": false,
      "contentType": false,
      "mimeType": "multipart/form-data",
      "data": form,
      success:(result)=>{
        console.log(result)
      },
      error:(err)=>{
        console.log(err.responseText)
      }
    })
    
    
});

$(document).on('click','.deleteCoupon',function () {// ERROR IN API

    let couponType=$(this).closest('tr').children('td:eq(0)').text();
    let couponCode=$(this).closest('tr').children('td:eq(1)').text();
    let couponId=$(this).closest('tr').attr('id');
    let coupon_status=$(this).closest('tr').children('td:eq(2)').text();
    if(coupon_status == "Active"){
        alert('Cant Delete Active Coupons');
        return false;
    }
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/delete_coupons",
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem('admin_uuid'),
            coupon_code :couponCode,
            coupon_type:couponType,
            coupon_id:couponId,
        },
        success:function (result) {
            console.log(result.status);
        },
        error:function (err) {
            alert(err.responseJSON.status);
        }
    })
});

$(document).on('click','.editCoupon',function () {
    let couponType=$(this).closest('tr').children('td:eq(0)').text();
    let couponCode=$(this).closest('tr').children('td:eq(1)').text();
    let couponId=$(this).closest('tr').attr('id');
    let coupon_status=$(this).closest('tr').children('td:eq(2)').text();
   
    $("#user_id").val(localStorage.getItem('admin_uuid'))
    $("#coupon_id").val(couponId);
    $("#coupon_code").val(couponCode)
    $('#section-settings').modal('show');

});

$(document).on('click','.editRedeem',function () {
    let redeemNo = $(this).closest('tr').children('td:eq(0)').text().trim()
    let redeemId=$(this).closest('tr').attr('id');
    let redeem_status=$(this).closest('tr').children('td:eq(7)').text().trim();
    // console.log(redeemId+' '+redeem_status);
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/get_redeem_point_barrier",
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem('admin_uuid')
        },
        success:function(result){
            // console.log(result.data);
           
            let pointsObj = result.data;
           
                let eachObj = pointsObj[redeemNo];
               
                $("#redeem_id_edit").val(eachObj.redeem_id);
                $("#redeem_status_edit").val(eachObj.redeem_status);
                $("#redeem_amounts_edit").val(eachObj.redeem_amount);
                $("#no_of_redeem_points_edit").val(eachObj.no_of_redeem_points);
                $("#redeem_activation_day_edit").val(eachObj.no_of_days_to_credit);
                $("#coupon_activation_date_edit").val(eachObj.activate_date.split("T")[0]);
                $("#coupon_expire_edit").val(eachObj.expire_date.split("T")[0]);
                $("#redeemTerms_edit").val(eachObj.terms_conditions);

               
           
        },
        error:function(err){
            console.log(err.responseText);
        }
    })
   
    $('#section-settings1').modal('show');

})

let minVal=1;
let count=1;
let objSlabSuper={};
let objSlabInner={};
$(document).on('click','#add_slab',function () {
   let localObj={};
    localObj.min_amount=minVal;
    localObj.max_amount=$('#max_amt').val();
    localObj.no_of_coupons=$('#coupon_used').val();
    $('#table_slab_append').append(`
    <tr>
        <td>`+minVal+`</td>
        <td>`+$('#max_amt').val()+`</td>
        <td>`+$('#coupon_used').val()+`</td>
    </tr>
    `)
    objSlabInner[count]=localObj;
    // console.log(objSlabInner);
    count=count+1;
    minVal=parseInt($('#max_amt').val())+1;
    // console.log(minVal);
});
$(document).on('submit','#slabForm',function (e) {
    e.preventDefault();
    objSlabSuper.slabs=objSlabInner;
    let finalObj=JSON.stringify(objSlabSuper);
    
       
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/add_coupon_slabs",
        type:"POST",
        dataType:"JSON",
        data:{
            admin_uuid:localStorage.getItem('admin_uuid'),
            coupon_code:$("#coupon_code").val().split(" ")[0],
            coupon_id:$("#coupon_code").val().split(" ")[1],
            coupon_slabs:finalObj
        },
        success:function (data) {
            console.log(data)
        },
        error:function (err) {
            console.log(err.responseText);
        }
    });
     minVal=1;
     count=1;
     objSlabSuper={};
     objSlabInner={};
     $('#table_slab_append').empty();
     $('#max_amt').val('');
     $('#coupon_used').val('');
});

$(document).on('submit',"#redeemForm",function (e) {
    e.preventDefault();
    let redeemObject ={
        admin_uuid:localStorage.getItem('admin_uuid'),
        redeem_amount:$("#redeem_amounts").val(),
        no_of_redeem_points:$("#no_of_redeem_points").val(),
        no_of_days_to_credit:$("#redeem_activation_day").val(),
        activate_date:$("#coupon_activation_date").val(),
        expire_date:$("#coupon_expire").val(),
        terms_conditions:$("#redeemTerms").val()
    }
    //console.log(redeemObject);
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/add_redeem_point_barrier",
        type:"POST",
        data: redeemObject,
        success:function (result) {
            console.log(result.status);
        },
        error:function (err) {
            console.log(err.responseText)
        }
    });
});

$(document).on('submit',"#redeemFormEdit",function (e) {
    e.preventDefault();
    let redeemObject ={
        admin_uuid:localStorage.getItem('admin_uuid'),
        redeem_id:$("#redeem_id_edit").val(),
        redeem_amount:$("#redeem_amounts_edit").val(),
        no_of_redeem_points:$("#no_of_redeem_points_edit").val(),
        no_of_days_to_credit:$("#redeem_activation_day_edit").val(),
        activate_date:$("#coupon_activation_date_edit").val(),
        expire_date:$("#coupon_expire_edit").val(),
        terms_conditions:$("#redeemTerms_edit").val()
    }
    console.log(redeemObject);
    $.ajax({
        url:"https://patashalafinder.in/admin_amrutha/update_redeem_point_barrier",
        type:"POST",
        data: redeemObject,
        success:function (result) {
            console.log(result.status);
            $('#section-settings1').modal('hide');
            $("#eco-redeem-points").trigger('click');
        },
        error:function (err) {
            console.log(err.responseText);
        }
    });
});
//couponEdit
let slider_images_arr_fr_web=[];
let slider_images_arr_fr_mob=[];

let image_slider_class_holder="image_div";

function displaySliderImageArrFr(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            // console.log(e.target.result);
            if(image_slider_class_holder=="image_div"){
                $(".image_upload_div_fr_append").remove();
                $("#"+image_slider_class_holder).append(`
                <div class="image_upload_div_fr_append">
                    <span class="uploadIcon"><img src="${e.target.result}" width="100%" height="100%" alt="" ></i></span>
                </div>
                `);
            }else{
                $(".image_upload_div_frMob_append").remove();
                $("#"+image_slider_class_holder).append(`
                <div class="image_upload_div_frMob_append">
                    <span class="uploadIcon"><img src="${e.target.result}" width="100%" height="100%" alt="" ></i></span>
                </div>
                `);
            }
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}

$(document).on('change','#trigger_slider_franchise_image_upload',function(){
    displaySliderImageArrFr(this);
    let filename=$("#trigger_slider_franchise_image_upload").get(0);
    let thatfile=filename.files;
    
    slider_images_arr_fr_web[0]=thatfile[0];
    
   
   
  
});

$(document).on('click','.image_upload_div_fr',function(){
    image_slider_class_holder="image_div";
    $('#trigger_slider_franchise_image_upload').trigger('click');
});

$(document).on('change','#trigger_slider_franchise_image_upload_mob',function(){
    displaySliderImageArrFr(this);
    let filename=$("#trigger_slider_franchise_image_upload_mob").get(0);
    let thatfile=filename.files;
    
        slider_images_arr_fr_mob[0]=thatfile[0];
    
   
   
  
});

$(document).on('click','.image_upload_div_frMob',function(){
    image_slider_class_holder="image_divMob";
    $('#trigger_slider_franchise_image_upload_mob').trigger('click');
});

$(document).on('click', '#franchisesliderUpload', function () {
   
   
    
    if(slider_images_arr_fr_web.length == 0 || slider_images_arr_fr_mob.length == 0){
        showAlertToast('Select Slider Images');
        return false;
    }
   
    let sliderData = new FormData();
        sliderData.append("admin_uuid",localStorage.getItem('admin_uuid'));
        sliderData.append("franchise_uuid",sessionStorage.getItem('franchiseUUID'));
        sliderData.append("image_position_no",$("#imagePosNo").val());
        sliderData.append("activate_date",$("#slider_activate_date").val());
        sliderData.append("expire_date",$("#slider_expiry_date").val());
        sliderData.append("image_link",$("#link_slider").val());
        sliderData.append("animation",$("#ani").val());
        sliderData.append("name",$("#name").val());
        sliderData.append("slider_image_web",slider_images_arr_fr_web[0]);
        sliderData.append("slider_image_mobile",slider_images_arr_fr_mob[0]);
   
    $.ajax({
        url:"https://patashalafinder.in/amrutha/upload_slider_images",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:sliderData,
        success:function (result) {
           
            slider_images_arr_fr_web=[];
            slider_images_arr_fr_mob=[];
            image_slider_class_holder="";
            $("#eco-slider-franchise-add").trigger('click');
        },
        error:function (err) {
            console.log(err.responseJSON.message);
        }
    });
});
let thisId = '';
$(document).on('click','.editFranchiseSliderWeb',function () {
    let thisData = $(this).attr('class').split(" ");
    thisId = thisData[1];
    let thisSliderPosition = thisData[4];
    let thisKey = ""+thisData[2]+" "+thisData[3]
    //console.log(thisData);
    let obj ={};
    let key='';
    $(".editFranchiseSliderWeb").each(function(){
        // console.log($(this).attr('class').split(" ")[2]);
        let actDate = $(this).attr('class').split(" ")[2];
        let expDate = $(this).attr('class').split(" ")[3];
        let sldNo =  $(this).attr('class').split(" ")[4];
        let newKey =  actDate+" "+expDate;
        
        if(key == newKey){
            obj[key].push(sldNo);
        }else{
            obj[newKey] = [sldNo];
            key = newKey;
        }
        
    });
    //console.log(JSON.stringify(obj));

    let getArr = obj[thisKey];
    //console.log(getArr.indexOf(thisSliderPosition))
    getArr.splice(getArr.indexOf(thisSliderPosition),1);
    $("#appendImagePosition").empty();
    $.each(getArr,(index)=>{
        $("#appendImagePosition").append(`
        <div><input type="radio" name="sliderPos" value="${getArr[index]}" checked><label>Position ${getArr[index]}</label></div>
        `);
    });
    $("#section-settings2").modal('show')
});
$(document).on('submit','#changeSliderPos',function (e) {
    e.preventDefault();
    let swapList =[
        {
            1:{'slider_id':thisId,"image_position_no":$("input[name='sliderPos']:checked").val()}
        }
    ]
        let sliderData = new FormData();
        sliderData.append("admin_uuid",localStorage.getItem('admin_uuid'));
        sliderData.append("images_swap_list",JSON.stringify(swapList));
        for(let key of sliderData.entries()){
            console.log(key[0]+":"+key[1])
        }
        $.ajax({
            url:"https://patashalafinder.in/admin_amrutha/updating_slider_images",
            type:"POST",
            dataType:"JSON",
            processData: false,
            contentType: false,
            data:sliderData,
            success:function (result) {
               
                thisId = ''
                 $("#section-settings2").modal('hide');
                 showAlertToast(result.message);
                 $('#eco-slider-franchise-add').trigger('click')
            },
            error:function (err) {
                console.log(err);
            }
        });
})
$(document).on('click','.editFranchiseSliderMob',function () {
    let thisData = $(this).attr('class').split(" ");
    //console.log(thisData);
    let obj ={};
    let key='';
    $(".editFranchiseSliderMob").each(function(){
        // console.log($(this).attr('class').split(" ")[2]);
        let actDate = $(this).attr('class').split(" ")[2];
        let expDate = $(this).attr('class').split(" ")[3];
        let sldNo =  $(this).attr('class').split(" ")[4];
        let newKey =  actDate+" "+expDate;
        
        if(key == newKey){
            obj[key].push(sldNo);
        }else{
            obj[newKey] = [sldNo];
            key = newKey;
        }
        
    })
    //console.log(JSON.stringify(obj))
})

let slider_images_arr=[];

let image_slider_id_holder="";

function displaySliderImageArr(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            // console.log(e.target.result);
            $("#"+image_slider_id_holder).attr('src',e.target.result);
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}
$(document).on('change','#trigger_slider_image_upload',function(){
    displaySliderImageArr(this);
    let filename=$("#trigger_slider_image_upload").get(0);
    let thatfile=filename.files;
    let checkMobOrWeb = image_slider_id_holder.split("_");
    if(checkMobOrWeb[1] == "web"){
        slider_images_arr[0]=thatfile[0];
    }else{
        slider_images_arr[1]=thatfile[0];
    }
   
   
  
});

$(document).on('click','.trigger-files',function(){
    image_slider_id_holder=$(this).attr('class').split(" ")[1];
    $('#trigger_slider_image_upload').trigger('click');
});

$(document).on('click', '.upload_slider', function () {
    // alert($(this).attr('id'));
    let linkId = $(this).attr('id');
    let sliderNo = linkId.split('r')[1];
    if(slider_images_arr.length == 0){
        alert('Select Slider Image');
        return false;
    }
    if($("#link_"+linkId).val() == ''){
        alert('Enter Link For Slider');
        return false;
    }

    let sliderData = new FormData();
        sliderData.append("admin_uuid",localStorage.getItem('admin_uuid'));
        sliderData.append("image_position_no",sliderNo);
        sliderData.append("activate_date",$("#slider_activate_date"+sliderNo).val());
        sliderData.append("expire_date",$("#slider_expiry_date"+sliderNo).val());
        sliderData.append("image_link",$("#link_"+linkId).val());
        sliderData.append("animation",$("#ani"+sliderNo).val());
        sliderData.append("name",$("#name"+sliderNo).val());
        sliderData.append("slider_image_web",slider_images_arr[0]);
        sliderData.append("slider_image_mobile",slider_images_arr[1]);

    $.ajax({
        url:"https://patashalafinder.in/amrutha/upload_index_slider_images",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:sliderData,
        success:function (result) {
            console.log(result);
            slider_images_arr=[];
            image_slider_id_holder=""
        },
        error:function (err) {
            alert(err.responseJSON.message);
        }
    });
});

let slideId;
let sliderType;
$(document).on('click','.editSlider',function () {
   let imageValue = $(this).attr('class').split(" ");
   slideId=imageValue[3];
   sliderType=imageValue[4];
   $("#modal-content").empty();
   $("#modal-content").append(`
 
   <button type="button" class="close closeModal" data-dismiss="modal" aria-hidden="true">&times;</button>

    <div class="modal-body" id="modal_body" style="min-height:300px">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <img class="img-rounded img-responsive trigger-files-slider-edit ${imageValue[2]} ${imageValue[3]}"
        alt="140x140" src="data/changeSlider.jpg" id="image_slider_edit"
        data-holder-rendered="true">
    <div class="controls img_link_slider_edit">
        <div class="input-group primary">
            <input type="text"
                class="form-control text-right link_slider_edit"
                id="link_slider_edit" placeholder="Link Of Slide"
                aria-describedby="basic-addon1">
            <span class="input-group-addon upload_slider_edit"
                id="slider_edit"><i class="fa fa-upload"></i></span>
        </div>
    </div>
</div>

    </div>

   `)
   $("#section-settings").modal('show');
});

let edit_slider_images_arr=[];

function displayNewSliderImageArr(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            // console.log(e.target.result);
            $("#image_slider_edit").attr('src',e.target.result);
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}

$(document).on('change','#trigger_slider_image_upload_edit',function(){
    displayNewSliderImageArr(this);
    let filename=$("#trigger_slider_image_upload_edit").get(0);
    let thatfile=filename.files;
    edit_slider_images_arr.push(thatfile[0]);
   
  
});

$(document).on('click','.trigger-files-slider-edit',function(){
  
    $('#trigger_slider_image_upload_edit').trigger('click');
});

$(document).on('click','#slider_edit',function () {
    
    if(edit_slider_images_arr.length == 0){
        alert('Select Slider Image');
        return false;
    }
    if($("#link_slider_edit").val() == ''){
        alert('Enter Link For Slider');
        return false;
    }

    let newSliderData = new FormData();
        newSliderData.append("user","Admin");
        newSliderData.append("slider_id",slideId);
        newSliderData.append("image_link",$("#link_slider_edit").val());
        if(sliderType == 'web'){
            newSliderData.append("slider_image_web",edit_slider_images_arr[0]);
        }else if(sliderType == 'mob'){
            newSliderData.append("slider_image_mobile",edit_slider_images_arr[0]);
        }
        

    $.ajax({
        url:"https://patashalafinder.in/amrutha/update_slider_images",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:newSliderData,
        success:function (result) {
            console.log(result);
            edit_slider_images_arr=[];
            slideId='';
            sliderType='';
        },
        error:function (err) {
            alert(err.responseJSON.message);
        }
    });
});

let slider_Badge_array_web = [];
let badge_image_id_no_web;
function displayBadgeImageArr(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
                // access image size here 
                //console.log(this.width);
                // if(this.width % this.height != 0){
                //     alert(this.width + "X" + this.height + " is not a valid image");
                    
                //     return false;
                // }else{
                    $("#imgDiv"+badge_image_id_no_web).empty();
                    $("#imgDiv"+badge_image_id_no_web).append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                    $(".image_div").append(`
                    <div class="image_upload_div" id="imgDiv${++badge_image_id_no_web}">
                    <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
        
                    </div>
                    `)
                //     $('.owl-carousel')
                //         .trigger('add.owl.carousel', [`<div class="single-partner-item">
                //                                     <a href="#"><img src="${this.src}" alt="image"></a>
                //                     </div>`])
                //         .trigger('refresh.owl.carousel');
                //         }
                // // $('#imgresizepreview, #profilepicturepreview').attr('src', this.src);
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}
$(document).on('change','#trigger_slider__badge_image_upload',function(e){
    displayBadgeImageArr(this);
    let filename=$("#trigger_slider__badge_image_upload").get(0);
    let thatfile=filename.files;
   
    slider_Badge_array_web.push(thatfile[0]);
    //console.log(slider_Badge_array_web);

    
});
$(document).on('click','.image_upload_div',function(){
    badge_image_id_no_web=$(this).attr('id').split("v")[1];
    $('#trigger_slider__badge_image_upload').trigger('click');
});
let slider_Badge_array_mob = [];
let badge_image_id_no_mob;
function displayBadgeImageArrMob(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
                // access image size here 
                //console.log(this.width);
                // if(this.width % this.height != 0){
                //     alert(this.width + "X" + this.height + " is not a valid image");
                   
                //     return false;
                // }else{
                    $("#imgDivMob"+badge_image_id_no_mob).empty();
                    $("#imgDivMob"+badge_image_id_no_mob).append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                    $(".image_divMob").append(`
                    <div class="image_upload_divMob" id="imgDivMob${++badge_image_id_no_mob}">
                    <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
        
                    </div>
                    `)
                    // $('.owl-carousel')
                    //     .trigger('add.owl.carousel', [`<div class="single-partner-item">
                    //                                 <a href="#"><img src="${this.src}" alt="image"></a>
                    //                 </div>`])
                    //     .trigger('refresh.owl.carousel');
                    //     }
                // $('#imgresizepreview, #profilepicturepreview').attr('src', this.src);
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}
$(document).on('change','#trigger_slider__badge_image_upload_mob',function(e){
    displayBadgeImageArrMob(this);
    let filename=$("#trigger_slider__badge_image_upload_mob").get(0);
    let thatfile=filename.files;
   
    slider_Badge_array_mob.push(thatfile[0]);
    //console.log(slider_Badge_array_mob);

    
});
$(document).on('click','.image_upload_divMob',function(){
    badge_image_id_no_mob=$(this).attr('id').split("b")[1];
    $('#trigger_slider__badge_image_upload_mob').trigger('click');
});

$(document).on('click','#addBadgeToSlider',function () {
    let form = new FormData();
    let badgeObj = {};
    //console.log(slider_Badge_array_mob,slider_Badge_array_web)
    if(slider_Badge_array_mob.length != slider_Badge_array_web.length){
        alert('Add equal no of images in both web and mobile');
        return false;
    }
   
    
    form.append("admin_uuid", localStorage.getItem('admin_uuid'));
    

    for(let i=0;i<slider_Badge_array_mob.length;i++){
    let eachObj={};
        let keyWeb = "slider_image_web" +i;
        let keyMob = "slider_image_mob" +i;
        //console.log(keyWeb,keyMob)
        eachObj["web"] = keyWeb;
        eachObj["mobile"] = keyMob;

       
        
        eachObj["animation"]="animation";
        badgeObj[i+1]=eachObj;
        
        form.append(keyWeb, slider_Badge_array_web[i]);
        form.append(keyMob, slider_Badge_array_mob[i]);
        //console.log(JSON.stringify(badgeObj))
    
       
    }
    form.append("slider_badge_list", JSON.stringify(badgeObj));
    // for(let key of form.entries()){
    //     console.log(key[0]+':'+key[1]);
    // }
    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/upload_badge_slider",
        "method": "POST",
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        "data": form,
        success:(result)=>{
            console.log(result)
            $("#eco-slider-badge-add").trigger('click');
        },
        error:(err)=>{
            console.log(err.responseText);
        }
})
    //return false;
    
})
let brand_array = [];
let brand_image_id_no;
function displayBrandImageArr(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
                // access image size here 
                //console.log(this.width);
                // if(this.width % this.height == 0){

                    $("#imgBrandDiv"+brand_image_id_no).empty();
                    $("#imgBrandDiv"+brand_image_id_no).append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                    $(".image_div_brand").append(`
                    <div class="image_upload_brand_div" id="imgBrandDiv${++brand_image_id_no}">
                    <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
        
                    </div>
                    `)
                   
                // }else{
                //     brand_array = [];
                //    alert('Brand logo Size is not proper')
                // }
                
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}
$(document).on('change','#trigger_brand_image_upload',function(e){
    displayBrandImageArr(this);
    let filename=$("#trigger_brand_image_upload").get(0);
    let thatfile=filename.files;
   
    brand_array.push(thatfile[0]);
    //console.log(brand_array);

    
});
$(document).on('click','.image_upload_brand_div',function(){
    brand_image_id_no=$(this).attr('id').split("v")[1];
    $('#trigger_brand_image_upload').trigger('click');
});
let sliderIceCreamHeaderArr = [];
let sliderIceCreamSideArr = [];
let headerOrSideImage;
function displaysliderIceCreamSide(input) {
    //console.log(headerOrSideImage)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
                // access image size here 
                console.log(this.width / this.height);
               // if(this.width % this.height != 0){

                    if(headerOrSideImage == "imageHeader"){
                        // if(this.width / this.height >= 2){
                            $("#imageHeader").empty();
                            $("#imageHeader").append(`<img src="${this.src}" width="100%" height="100%">`);
                            $('.image_header_div').find('div').not('#imageHeader').remove();
                            $(".image_header_div").append(`
                            <div class="image_upload_sliderIceCream imageHeader">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100px" height="100px" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Header Image Size is not proper')
                        // }
                       
                    }else if(headerOrSideImage == "imageSide"){
                        // if(this.width % this.height == 0){
                            $("#imageSide").empty();
                            $("#imageSide").append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                            $('.image_side_div').find('div').not('#imageSide').remove();
                            $(".image_side_div").append(`
                            <div class="image_upload_sliderIceCream imageSide">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Size Image Size is not proper')
                        // }
                       
                    }
                    
                   
                // }else{
                   
                // }
                
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}

$(document).on('change','#trigger_sliderIceCream_image_upload',function(e){
   

    let filename=$("#trigger_sliderIceCream_image_upload").get(0);
    let thatfile=filename.files;
   
    
    headerOrSideImage == "imageHeader" ?  sliderIceCreamHeaderArr[0]=thatfile[0] :  sliderIceCreamSideArr[0]=thatfile[0]

    console.log(sliderIceCreamHeaderArr,sliderIceCreamSideArr);

    displaysliderIceCreamSide(this);
});

$(document).on('click','.image_upload_sliderIceCream',function(){
    headerOrSideImage=$(this).attr('class').split(" ")[1];
    $('#trigger_sliderIceCream_image_upload').trigger('click');
});

let sliderIceCreamHeaderArrMob = [];
let sliderIceCreamSideArrMob = [];
let headerOrSideImageMob;
function displaysliderIceCreamSideMob(input) {
    //console.log(headerOrSideImage)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
                // access image size here 
                console.log(this.width / this.height);
               // if(this.width % this.height != 0){

                    if(headerOrSideImageMob == "imageHeader_mob"){
                        // if(this.width / this.height >= 2){
                            $("#imageHeader_mob").empty();
                            $("#imageHeader_mob").append(`<img src="${this.src}" width="100%" height="100%">`);
                            $('.image_header_div_mob').find('div').not('#imageHeader_mob').remove();
                            $(".image_header_div_mob").append(`
                            <div class="image_upload_sliderIceCream_mob imageHeader_mob">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100px" height="100px" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Header Image Size is not proper')
                        // }
                       
                    }else if(headerOrSideImageMob == "imageSide_mob"){
                        // if(this.width % this.height == 0){
                            $("#imageSide_mob").empty();
                            $("#imageSide_mob").append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                            $('.image_side_div_mob').find('div').not('#imageSide_mob').remove();
                            $(".image_side_div_mob").append(`
                            <div class="image_upload_sliderIceCream_mob imageSide_mob">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Size Image Size is not proper')
                        // }
                       
                    }
                    
                   
                // }else{
                   
                // }
                
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}

$(document).on('change','#trigger_sliderIceCream_image_upload_mob',function(e){
   

    let filename=$("#trigger_sliderIceCream_image_upload_mob").get(0);
    let thatfile=filename.files;
   
    
    headerOrSideImageMob == "imageHeader_mob" ?  sliderIceCreamHeaderArrMob[0]=thatfile[0] :  sliderIceCreamSideArrMob[0]=thatfile[0]

    console.log(sliderIceCreamHeaderArrMob,sliderIceCreamSideArrMob);

    displaysliderIceCreamSideMob(this);
});

$(document).on('click','.image_upload_sliderIceCream_mob',function(){
    headerOrSideImageMob=$(this).attr('class').split(" ")[1];
    $('#trigger_sliderIceCream_image_upload_mob').trigger('click');
});

$(document).on('click','#uploadIceCream',function () {

    if(sliderIceCreamHeaderArr.length == 0 || sliderIceCreamSideArr.length == 0 || sliderIceCreamHeaderArrMob.length == 0 || sliderIceCreamSideArrMob.length == 0){
        alert('Select All Slider Images');
        return false;
    }
   
    let sliderData = new FormData();
        sliderData.append("admin_uuid",localStorage.getItem('admin_uuid'));
        sliderData.append("position",$("#selectPosition").val());
        sliderData.append("activate_date",$("#sliderIcecreamTypeAct").val());
        sliderData.append("expire_date",$("#sliderIcecreamTypeExp").val());
        sliderData.append("animation",$("#animatIce").val());
        sliderData.append("header_name",$("#sliderHeaderName").val());
        sliderData.append("header_image_web",sliderIceCreamHeaderArr[0]);
        sliderData.append("header_image_mobile",sliderIceCreamHeaderArrMob[0]);
        sliderData.append("side_image_web",sliderIceCreamSideArr[0]);
        sliderData.append("side_image_mobile",sliderIceCreamSideArrMob[0]);
   
    $.ajax({
        url:"https://patashalafinder.in/amrutha/upload_structure_slider",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:sliderData,
        success:function (result) {
           console.log(result)
           sliderIceCreamHeaderArrMob = [];
           sliderIceCreamSideArrMob = [];
            headerOrSideImageMob;
            sliderIceCreamHeaderArr = [];
            sliderIceCreamSideArr = [];
            headerOrSideImage;
            // $("#eco-slider-franchise-add").trigger('click');
        },
        error:function (err) {
            console.log(err.responseText);
        }
    });
});

let sliderPrdtSumHeaderArr = [];
let sliderPrdtSumBackgroundArr = [];
let headerOrSlideBackgroundImage;
function displaysliderPrdtSumm(input) {
    //console.log(headerOrSideImage)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
                // access image size here 
                console.log(this.width / this.height);
               // if(this.width % this.height != 0){

                    if(headerOrSlideBackgroundImage == "imagePrdtSumHeader"){
                        // if(this.width / this.height >= 2){
                            $("#imagePrdtSumHeader").empty();
                            $("#imagePrdtSumHeader").append(`<img src="${this.src}" width="100%" height="100%">`);
                            $('.image_header_prdt_sum_div').find('div').not('#imagePrdtSumHeader').remove();
                            $(".image_header_prdt_sum_div").append(`
                            <div class="image_upload_slider_prdt_summary imagePrdtSumHeader">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100px" height="100px" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Header Image Size is not proper')
                        // }
                       
                    }else if(headerOrSlideBackgroundImage == "imagePrdtSumbackground"){
                        // if(this.width % this.height == 0){
                            $("#imagePrdtSumbackground").empty();
                            $("#imagePrdtSumbackground").append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                            $('.image_prdt_sum_background_div').find('div').not('#imagePrdtSumbackground').remove();
                            $(".image_prdt_sum_background_div").append(`
                            <div class="image_upload_slider_prdt_summary imagePrdtSumbackground">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Size Image Size is not proper')
                        // }
                       
                    }
                    
                   
                // }else{
                   
                // }
                
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}
$(document).on('change','#trigger_slider_prdt_summary_image_upload',function(e){
   

    let filename=$("#trigger_slider_prdt_summary_image_upload").get(0);
    let thatfile=filename.files;
    
    headerOrSlideBackgroundImage == "imagePrdtSumHeader" ?  sliderPrdtSumHeaderArr[0]=thatfile[0] :  sliderPrdtSumBackgroundArr[0]=thatfile[0]

    console.log(sliderPrdtSumHeaderArr,sliderPrdtSumBackgroundArr);
    displaysliderPrdtSumm(this);
    
});
$(document).on('click','.image_upload_slider_prdt_summary',function(){
    headerOrSlideBackgroundImage=$(this).attr('class').split(" ")[1];
    $('#trigger_slider_prdt_summary_image_upload').trigger('click');
});

let sliderPrdtSumHeaderArrMob = [];
let sliderPrdtSumBackgroundArrMob = [];
let headerOrSlideBackgroundImageMob;
function displaysliderPrdtSummMob(input) {
    //console.log(headerOrSideImage)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
                // access image size here 
                //console.log(this.width / this.height);
               // if(this.width % this.height != 0){

                    if(headerOrSlideBackgroundImageMob == "imagePrdtSumHeader_mob"){
                        // if(this.width / this.height >= 2){
                            $("#imagePrdtSumHeader_mob").empty();
                            $("#imagePrdtSumHeader_mob").append(`<img src="${this.src}" width="100%" height="100%">`);
                            $('.image_header_prdt_sum_div_mob').find('div').not('#imagePrdtSumHeader_mob').remove();
                            $(".image_header_prdt_sum_div_mob").append(`
                            <div class="image_upload_slider_prdt_summary_mob imagePrdtSumHeader_mob">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100px" height="100px" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Header Image Size is not proper')
                        // }
                       
                    }else if(headerOrSlideBackgroundImageMob == "imagePrdtSumbackground_mob"){
                        // if(this.width % this.height == 0){
                            $("#imagePrdtSumbackground_mob").empty();
                            $("#imagePrdtSumbackground_mob").append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                            $('.image_prdt_sum_background_div_mob').find('div').not('#imagePrdtSumbackground_mob').remove();
                            $(".image_prdt_sum_background_div_mob").append(`
                            <div class="image_upload_slider_prdt_summary_mob imagePrdtSumbackground_mob">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Size Image Size is not proper')
                        // }
                       
                    }
                    
                   
                // }else{
                   
                // }
                
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}
$(document).on('change','#trigger_slider_prdt_summary_image_upload_mob',function(e){
    

    
    let filename=$("#trigger_slider_prdt_summary_image_upload_mob").get(0);
    let thatfile=filename.files;
    
    headerOrSlideBackgroundImageMob == "imagePrdtSumHeader_mob" ?  sliderPrdtSumHeaderArrMob[0]=thatfile[0] :  sliderPrdtSumBackgroundArrMob[0]=thatfile[0]

    //console.log(sliderPrdtSumHeaderArrMob,sliderPrdtSumBackgroundArrMob);
    displaysliderPrdtSummMob(this);
    
});
$(document).on('click','.image_upload_slider_prdt_summary_mob',function(){
    headerOrSlideBackgroundImageMob=$(this).attr('class').split(" ")[1];
    $('#trigger_slider_prdt_summary_image_upload_mob').trigger('click');
});

$(document).on('click','#uploadPrdtSum',function () {
    if(sliderPrdtSumHeaderArrMob.length == 0 || sliderPrdtSumBackgroundArrMob.length == 0 || sliderPrdtSumHeaderArr.length == 0 || sliderPrdtSumBackgroundArr.length == 0){
        alert('Select All Slider Images');
        return false;
    }
   
    let sliderData = new FormData();
        sliderData.append("admin_uuid",localStorage.getItem('admin_uuid'));
        sliderData.append("activate_date",$("#sliderPrdtSumAct").val());
        sliderData.append("expire_date",$("#sliderPrdtSumExp").val());
        sliderData.append("animation",$("#animatePrdtSum").val());
        sliderData.append("header_name",$("#sliderHeaderPrdtSumName").val());
        sliderData.append("header_image_web",sliderPrdtSumHeaderArr[0]);
        sliderData.append("header_image_mobile",sliderPrdtSumHeaderArrMob[0]);
        sliderData.append("image_web",sliderPrdtSumBackgroundArr[0]);
        sliderData.append("image_mobile",sliderPrdtSumBackgroundArrMob[0]);
   
    $.ajax({
        url:"https://patashalafinder.in/amrutha/upload_category_structure_slider",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:sliderData,
        success:function (result) {
           console.log(result)
           sliderPrdtSumHeaderArr = [];
           sliderPrdtSumHeaderArrMob = [];
           headerOrSlideBackgroundImageMob;
            sliderPrdtSumBackgroundArr = [];
            sliderPrdtSumBackgroundArrMob = [];
            headerOrSlideBackgroundImage;
            // $("#eco-slider-franchise-add").trigger('click');
        },
        error:function (err) {
            console.log(err.responseText);
        }
    });
})
let color_slider_array_web = [];
let color_slider_array_mob = [];
let color_slider_web_or_mob='';
let color_slider_image_id_no_web;
let color_slider_image_id_no_mob;

$(document).on("click","input[name='staticStructure']",function () {
    
    if($("input[name='staticStructure']:checked").val() != "BOTTOM"){
        $(".notBottom").show();
        $(".bottom").hide();
    }else{
        $(".bottom").show();
        $(".notBottom").show();
        let formData = new FormData();
        formData.append("admin_uuid", localStorage.getItem('admin_uuid'));
       
        $.ajax({
           "async": true,
           "crossDomain": true,
           "url": "https://patashalafinder.in/amrutha/get_product",
           "method": "POST",
           "data": formData,
           "processData": false,
           "contentType": false,
           "mimeType": "multipart/form-data",
           success: (result) => {
   
               
               let objFull = JSON.parse(result).data.Approved;
               
               //console.log(objFull);
               $(".select2-search-choice-close").trigger('click');
               $("#s2example-1").empty();
                $("#s2example-1").append('<optgroup label="Product" id="appendOptionProduct"></optgroup>')
               $.each(objFull, (key, value) => {
   
                     
                           $("#appendOptionProduct").append(`
                           <option value="${value.productId}">${value.product_name}</option>
                           `);
                       
                      
                  
   
               });
               
   
           },
           error: (err) => {
               console.log(err.responseText);
           }
       })
    }
});

let structureSliderArr = [];

let structureSliderImageWebMob;
function displaysliderstructureSlider(input) {
    //console.log(headerOrSideImage)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
                // access image size here 
                //console.log(this.width / this.height);
               // if(this.width % this.height != 0){

                    if(structureSliderImageWebMob == "imageStructureSlider"){
                        // if(this.width / this.height >= 2){
                            $("#imageStructureSlider").empty();
                            $("#imageStructureSlider").append(`<img src="${this.src}" width="100%" height="100%">`);
                            $('.image_structure_slider_web').find('div').not('#imageStructureSlider').remove();
                            $(".image_structure_slider_web").append(`
                            <div class="image_upload_slider_structure_slider imageStructureSlider">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100px" height="100px" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Header Image Size is not proper')
                        // }
                       
                    }else if(structureSliderImageWebMob == "imageStructureSlider_mob"){
                        // if(this.width % this.height == 0){
                            $("#imageStructureSlider_mob").empty();
                            $("#imageStructureSlider_mob").append(`<img src="${this.src}" width="100%" height="100%" >`);
                            $('.image_structure_slider_mob').find('div').not('#imageStructureSlider_mob').remove();
                            $(".image_structure_slider_mob").append(`
                            <div class="image_upload_slider_structure_slider imageStructureSlider_mob">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
                
                            </div>
                            `)
                        // }
                        // else{
                        //     alert('Size Image Size is not proper')
                        // }
                       
                    }
                    
                   
                // }else{
                   
                // }
                
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}

$(document).on('change','#trigger_slider_structure_slider_image_upload',function(e){
   
    let filename=$("#trigger_slider_structure_slider_image_upload").get(0);
    let thatfile=filename.files;
    structureSliderImageWebMob == "imageStructureSlider" ?  structureSliderArr[0]=thatfile[0] :  structureSliderArr[1]=thatfile[0]
    //console.log(structureSliderArr);
    displaysliderstructureSlider(this);
    
});

$(document).on('click','.image_upload_slider_structure_slider',function(){
    structureSliderImageWebMob=$(this).attr('class').split(" ")[1];
    $('#trigger_slider_structure_slider_image_upload').trigger('click');
});

$(document).on('click','#uploadStructureSlider',function () {
    if($("#staticStructureName").val() == null){
        showAlertToast("Set Name.");
        return false;
    }
    if($("#staticStructureLink").val() == null){
        showAlertToast("Set Link.");
        return false;
    }
    if($("#staticStructureActivationDate").val() == ""){
        showAlertToast("Select Activation Date.");
        return false;
    }
    if($("#staticStructureExpiryDate").val() == ""){
        showAlertToast("Select Expiry Date.");
        return false;
    }
    if(structureSliderArr.length != 2){
        showAlertToast("Select Images.");
        return false;
    }
   
    if($("#staticStructureAnimate").val() == ""){
        showAlertToast("Set Animation.");
        return false;
    }
    let form = new FormData();
    form.append("admin_uuid", localStorage.getItem('admin_uuid'));
    form.append("name", $("#staticStructureName").val());
    form.append("main_link", $("#staticStructureLink").val());
    form.append("activate_date", $("#staticStructureActivationDate").val());
    form.append("expire_date", $("#staticStructureExpiryDate").val());
    form.append("main_image_web", structureSliderArr[0]);
    form.append("main_image_mobile", structureSliderArr[1]);
    form.append("animation", $("#staticStructureAnimate").val());
    form.append("position", $("input[name='staticStructure']:checked").val());
   
    if($("input[name='staticStructure']:checked").val() == "BOTTOM"){
        let imgListArr=[];
        let productArr = $("#s2example-1").val().map((item)=>{return item});
        if(color_slider_array_web.length != color_slider_array_mob.length){
            showAlertToast("Select Equal Images In Both");
            return false
        }
        if(productArr.length != color_slider_array_mob.length){
            showAlertToast("Select Same No Images as Many as Product Selected");
            return false
        }
        for(let i =0; i<productArr.length;i++){
            let eachObj ={};
            eachObj={
                "web":  "keyWeb"+i,
                "mobile":"keyMob"+i,
                "product":productArr[i],
                "animation":$("#staticStructureAnimate").val()
            }
            
            imgListArr.push(eachObj)
        }
        form.append("image_list", JSON.stringify(imgListArr));
        for(let i =0; i<productArr.length;i++){
           
            form.append("keyWeb"+i, color_slider_array_web[i]);
            form.append("keyMob"+i, color_slider_array_mob[i]);
            
        }
    }
    // for(let key of form.entries()){
    //     console.log(key[0]+":"+key[1])
    // }
    //  return false
    $.ajax({
        url:"https://patashalafinder.in/amrutha/upload_static_structure_slider",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:form,
        success: (result) => {
            console.log(result.message);
            structureSliderArr = [];
            structureSliderImageWebMob;
            color_slider_array_web = [];
            color_slider_array_mob = [];
            color_slider_web_or_mob='';
            color_slider_image_id_no_web;
            color_slider_image_id_no_mob;
            $(".form-control").val('');
            $(".select2-search-choice-close").trigger('click');
            showAlertToast(result.message)
        },
        error: (err) => {
            console.log(err.responseText);
            showAlertToast(err.responseText)
        }
    })
})



function displayColorSliderImageArr(input) {
    // console.log(input)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
              
                if(color_slider_web_or_mob == "imageColorStructureSlider"){
                    $("#imageColorStructureSliderX"+color_slider_image_id_no_web).empty();
                    $("#imageColorStructureSliderX"+color_slider_image_id_no_web).append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                    $(".image_color_structure_slider_web").append(`
                    <div class="image_color_upload_structure_slider imageColorStructureSlider" id="imageColorStructureSliderX${++color_slider_image_id_no_web}">
                    <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
        
                    </div>
                    `)
                }
                else if(color_slider_web_or_mob == "imageStructureSlider_mob"){
                    $("#imageStructureSlider_mobX"+color_slider_image_id_no_mob).empty();
                    $("#imageStructureSlider_mobX"+color_slider_image_id_no_mob).append(`<img src="${this.src}" width="100%" height="100%" style="border-radius: 50%;">`);
                    $(".image_color_structure_slider_mob").append(`
                    <div class="image_color_upload_structure_slider imageStructureSlider_mob" id="imageStructureSlider_mobX${++color_slider_image_id_no_mob}">
                    <span class="uploadIcon"><img src="data/addBadge.png" width="100%" height="100%" alt=""></i></span>
        
                    </div>
                    `)
                }
                    
              
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}
$(document).on('change','#trigger_color_structure_slider_image_upload',function(e){
    
    let filename=$("#trigger_color_structure_slider_image_upload").get(0);
    let thatfile=filename.files;
   
    color_slider_web_or_mob == "imageColorStructureSlider" ? color_slider_array_web.push(thatfile[0]) : color_slider_array_mob.push(thatfile[0]);
    console.log(color_slider_array_web,color_slider_array_mob);
    displayColorSliderImageArr(this);
    
});
$(document).on('click','.image_color_upload_structure_slider',function(){
    color_slider_web_or_mob=$(this).attr('class').split(" ")[1];
    if(color_slider_web_or_mob == "imageColorStructureSlider"){
        color_slider_image_id_no_web=$(this).attr('id').split("X")[1];
    }else{
        color_slider_image_id_no_mob=$(this).attr('id').split("X")[1];
    }
    
    $('#trigger_color_structure_slider_image_upload').trigger('click');
});

$(document).on('click','#uploadColorStructureSlider',function () {
    
    if($("#colorStructureName").val() == ""){
        showAlertToast("Set Name.");
        return false;
    }
    else if($("#colorStructureLink").val() == ""){
        showAlertToast("Set Link.");
        return false;
    }
    else if($("#colorStructureActivationDate").val() == ""){
        showAlertToast("Select Activation Date.");
        return false;
    }
    else if($("#colorStructureExpiryDate").val() == ""){
        showAlertToast("Select Expiry Date.");
        return false;
    }
    else if($("#colorStructureAnimate").val() == ""){
        showAlertToast("Set Animation.");
        return false;
    }
    else if(color_slider_array_web.length != color_slider_array_mob.length){
        showAlertToast("Add Equal Images In Both Mobile And Web.");
        return false;
    }
    let form = new FormData();
    form.append("admin_uuid", localStorage.getItem('admin_uuid'));
    form.append("name", $("#colorStructureName").val());
    form.append("main_link", $("#colorStructureLink").val());
    form.append("activate_date", $("#colorStructureActivationDate").val());
    form.append("expire_date", $("#colorStructureExpiryDate").val());
   
    form.append("animation", $("#colorStructureAnimate").val());
    form.append("position", $("input[name='colorStructure']:checked").val());
    let imgListArr = [];
    for(let i=0;i<color_slider_array_web.length;i++){
        let eachObj ={};
        eachObj={
            "web":  "keyWeb"+i,
            "mobile":"keyMob"+i
        }
        
        imgListArr.push(eachObj)
    }
    form.append("image_list",JSON.stringify(imgListArr));
    for(let i=0;i<color_slider_array_web.length;i++){
      
        form.append("keyWeb"+i, color_slider_array_web[i]);
        form.append("keyMob"+i, color_slider_array_mob[i]);
       
    }
  
    $.ajax({
        url:"https://patashalafinder.in/amrutha/upload_color_slider",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:form,
        success: (result) => {
            // console.log(result.message);
             color_slider_array_web = [];
             color_slider_array_mob = [];
             color_slider_web_or_mob='';
             color_slider_image_id_no_web;
             color_slider_image_id_no_mob;
             $(".form-control").val('');
            showAlertToast(result.message)
        },
        error: (err) => {
            // console.log(err.responseText);
            showAlertToast(err.responseText)
        }
    })
})

$(document).on('submit','#addBrandForm',function (e) {
    e.preventDefault();
    if(brand_array.length == 0){
        alert("Select Brand Logo");
        return false;
    }
    let formData = new FormData();
    formData.append('admin_uuid',localStorage.getItem('admin_uuid'));
    formData.append('brand_name',$("#brand_add").val());
    formData.append('fssai_number',$("#brand_fssi").val());
    formData.append('brand_logo',brand_array[0]);
    $.ajax({
        url: "https://patashalafinder.in/amrutha/add_brand",
        type: "POST",
        processData: false,
        contentType: false,
        data: formData,
        success: function (result) {
            // alert(result);
            console.log(result);
            document.addBrandForm.reset();
            brand_array=[];
        },
        error: function (err) {
            console.log(err.responseJSON);
            document.addBrandForm.reset();
            brand_array=[];
            alert(err.responseJSON);
        }
    });
});
$(document).on('click','.activateBrand',function () {
    let brandUUID = $(this).attr('id');
   // alert(brandUUID);
    $.ajax({
        url:'https://patashalafinder.in/amrutha/update_brand',
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem('admin_uuid'),
            brand_uuid:brandUUID,
            brand_status:'true'
        },
        success: (result) => {
            console.log(result);
            $("#eco-brands").trigger('click');
        },
        error: (err) => {
            console.log(err);
        }
    });
});
$(document).on('click','.deactivateBrand',function () {
    let brandUUID = $(this).attr('id');
    // alert(brandUUID);
     $.ajax({
         url:'https://patashalafinder.in/amrutha/update_brand',
         type:"POST",
         data:{
             admin_uuid:localStorage.getItem('admin_uuid'),
             brand_uuid:brandUUID,
             brand_status:'false'
         },
         success: (result) => {
             console.log(result);
             $("#eco-brands").trigger('click');
         },
         error: (err) => {
             console.log(err);
         }
     });
});

$(document).on('submit','#addBrandSliderForm',function (e) {
    e.preventDefault();
    let form = new FormData();
    let brandObj = {};
    let brandArr = [$("#sliderLink").val(),$("#sliderBrandAnimation").val()];
    let brand = $("#selectBrand").val();
   
    brandObj[brand]=brandArr;

    form.append("admin_uuid", localStorage.getItem('admin_uuid'));
    form.append("slider_brand_list", JSON.stringify(brandObj));

    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/upload_update_brand_slider",
        "method": "POST",
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        "data": form,
        success:(result)=>{
            //console.log(result);
            let res = JSON.parse(result).message;
            showAlertToast(res);
            $("#eco-slider-brand-add").trigger('click');
        },
        error:(err)=>{
            console.log(err.responseText);
        }
})

});

$(document).on('click',"#deleteBrandSliderImage",function (e) {
    const brandSliderId = $(this).attr('class').split(" ")[1];
    //console.log(brandSliderId);
    let form = new FormData();
    form.append("admin_uuid", localStorage.getItem('admin_uuid'));
    form.append("slider_brand_uuid", brandSliderId);

    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/delete_brand_slider",
        "method": "POST",
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        "data": form,
        success:(result)=>{
            //console.log(result);
            let res = JSON.parse(result).message;
            showAlertToast(res);
            $("#eco-slider-brand-add").trigger('click');
        },
        error:(err)=>{
            let res = JSON.parse(err.responseText).message;
            showAlertToast(res);
            console.log(err.responseText);
        }
})
})
$(document).on('click','#hotDeal', (e) => {

    if($("#franchiseHot").val() == null){
        showAlertToast("Select Franchise.");
        return false;
    }
    if($("#prdt_sub_cat_hot").val() == null){
        showAlertToast("Select Product.");
        return false;
    }
    if($("#prdt_hot_off_start").val() == ""){
        showAlertToast("Select Hot Offer Start Date.");
        return false;
    }
    if($("#prdt_hot_off_end").val() == ""){
        showAlertToast("Select Hot Offer End Date.");
        return false;
    }
    if($("#prdt_hot_off_start_time").val() == ""){
        showAlertToast("Select Hot Offer Start Time.");
        return false;
    }
    if($("#prdt_hot_off_end_time").val() == ""){
        showAlertToast("Select Hot Offer End Time.");
        return false;
    }
    if($("#prdt_hot_quantity").val() == ""){
        showAlertToast("Select Hot Offer Product Quantity.");
        return false;
    }

    let form = new FormData();
    form.append("admin_uuid", localStorage.getItem('admin_uuid'));
    form.append("franchise_uuid", $("#franchiseHot").val());
    form.append("hotdeal_start_date", $("#prdt_hot_off_start").val());
    form.append("hotdeal_end_date", $("#prdt_hot_off_end").val());
    form.append("hotdeal_start_time", $("#prdt_hot_off_start_time").val());
    form.append("hotdeal_end_time", $("#prdt_hot_off_end_time").val());
    form.append("stock", $("#prdt_hot_quantity").val());
    form.append("productId", $("#prdt_sub_cat_hot").val());

    if($("input[name='hotOffer']:checked").val() == "PERCENT"){
        form.append("hotdeal_type", "PERCENT");
        if($("#prdt_hot_percent").val() == ""){
            showAlertToast("Set Hot Offer Percent.");
            return false;
        }
        form.append("hotdeal_percent", $("#prdt_hot_percent").val());
    }else{
        form.append("hotdeal_type", "AMOUNT");
        if($("#prdt_hot_amount").val() == ""){
            showAlertToast("Set Hot Offer Amount.");
            return false;
        }
        form.append("hotdeal_amount", $("#prdt_hot_amount").val());
        
    }
    $.ajax({
        url:"https://patashalafinder.in/amrutha/add_hotdeal",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:form,
        success: (result) => {
            // console.log(result.message);
            $("#eco-product-add-hot").trigger('click')
            showAlertToast(result.message)
        },
        error: (err) => {
            // console.log(err.responseText.message);
            showAlertToast(err.responseText)
        }
    })
});

let currentPrdtId='';
$(document).on('click', ".editListingLinkPrdt", function () {

    let btnValue = $(this).attr('id').split("_");
    let status = btnValue[2];
    let keyNo = btnValue[1];
    currentPrdtId = btnValue[0];
    if(status == "Approved"){
        $("#rejectPrdtBtn").hide();
        $("#approvePrdtBtn").show();
        $("#rejectPrdtBtn").attr('disabled','disabled')
        $("#approvePrdtBtn").attr('disabled','disabled')
    }else if(status == "Rejected"){
        $("#rejectPrdtBtn").show();
        $("#approvePrdtBtn").hide();
        $("#rejectPrdtBtn").attr('disabled','disabled')
        $("#approvePrdtBtn").attr('disabled','disabled')
    }else{
        $("#rejectPrdtBtn").show();
        $("#approvePrdtBtn").show();
        $("#rejectPrdtBtn").removeAttr('disabled')
        $("#approvePrdtBtn").removeAttr('disabled')
    }
    $.ajax({
        url: "https://patashalafinder.in/amrutha/get_product",
        type: "POST",
        data: {
            admin_uuid: localStorage.getItem('admin_uuid'),
        },
        success: function (result) {
           
            let data = result.data;
            let dataUse = data[status];
            let finalData = dataUse[keyNo]
            
            $("#prdt_name").val(finalData.product_name);
            $("#prdt_brand").val(finalData.brand)
            $("#category").val(finalData.category)
            $("#subCat").val(finalData.subcategory)
            $("#food_preference").val(finalData.food_preference)
            $("#mrp").val(finalData.mrp)
            $("#sp").val(finalData.selling_price)
            $("#weight").val(''+finalData.weight+' '+finalData.unit)
            $("#hsn").val(finalData.hsn)
            $("#gst").val(finalData.tax_code)
            $("#amount_ex_gst").val(finalData.amount_excluding_gst)
            $("#vendor_name").val(''+finalData.added_by_first_name+' '+finalData.added_by_last_name)
            $("#vendor_email").val(finalData.added_by_email)
        
            $("#prdt_image_web_aprv").attr('src',"https://patashalafinder.in/media/"+finalData.image_web)
            $("#prdt_image_web_aprv_view").attr('href',"https://patashalafinder.in/media/"+finalData.image_web)
           
            $("#prdt_image_mob_aprv").attr('src',"https://patashalafinder.in/media/"+finalData.image_mobile)
            $("#prdt_image_mob_aprv_view").attr('href',"https://patashalafinder.in/media/"+finalData.image_mobile)
        },
        error: function (err) {
            console.log(err.responseText)
        }
    });
    $(".tableDivFloat").slideDown(500);
});
$(document).on('click','.btnMar',function () {
    
    let actionStatus = $(this).val();

    $.ajax({
        url:'https://patashalafinder.in/amrutha/update_product',
        type:"POST",
        data:{
            admin_uuid:localStorage.getItem('admin_uuid'),
            productId : currentPrdtId,
            approval:actionStatus,
            mrp:$("#mrp").val(),
            selling_price:$("#sp").val()
        },
        success: (result) => {
            showAlertToast(result.message);
            currentPrdtId='';
            $("#eco_approve_products").trigger('click');
        },
        error: (err) => {
            currentPrdtId='';
            showAlertToast(err.responseText);
        }
    });
})

$(document).on('click','#uploadPolicy',function () {
    if($("#csvPolicy").get(0)){
        alert('Select Csv File.');
        return false;
    }
    let fileGet = $("#csvPolicy").get(0);
    let csvFile = fileGet.files;

    let formdata = new FormData();
    formdata.append("admin_uuid",localStorage.getItem('admin_uuid'));
    formdata.append("policy_file",csvFile[0]);
   

$.ajax({
    url:"https://patashalafinder.in/amrutha/add_users_policies",
    type:"POST",
    dataType:"JSON",
    processData: false,
    contentType: false,
    data:formdata,
    success:function (result) {
       console.log(result)
       
    },
    error:function (err) {
        console.log(err.responseText);
    }
});
});
let policyId='';
let policyType = '';
$(document).on('click','.editPolicy',function () {
     policyId = $(this).attr('id').split("_")[0];
    policyType = $(this).attr('id').split("_")[1];
    $("#modal-content").empty();
    $("#modal-content").append(`
  
    <button type="button" class="close closeModal" data-dismiss="modal" aria-hidden="true">&times;</button>
 
     <div class="modal-body" id="modal_body" style="min-height:150px">
 
     <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
     <div class="form-group">
                                        <label class="form-label" for="editPolicyText">Edit Policy Text</label>
                                        <div class="controls">
                                            <input type="text" class="form-control" id="editPolicyText"/>
                                           
                                        </div>
                                </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success pull-right" id="updatePolicy">Update</button>
    </div>
 </div>
 
     </div>
 
    `)
    $("#section-settings").modal('show');
 });
 $(document).on('click','#updatePolicy',function () {
     if($("#editPolicyText").val() == ""){
         showAlertToast('Enter New Text.');
         return false;
     }
     let formdata = new FormData();
     formdata.append("admin_uuid",localStorage.getItem('admin_uuid'));
     formdata.append("policy_id",policyId);
     formdata.append(""+policyType+"_policy",$("#editPolicyText").val());
    
 $.ajax({
     url:"https://patashalafinder.in/amrutha/update_users_policies",
     type:"POST",
     dataType:"JSON",
     processData: false,
     contentType: false,
     data:formdata,
     success:function (result) {
       
        policyId='';
        policyType = '';
        showAlertToast(result.message);
        $("#section-settings").modal('hide');
     },
     error:function (err) {
        showAlertToast(err.responseText.message);
     }
 });
 })

$(document).on('change','#brandList',function () {
     
     if($('#franchiseList').val() == null){
        showAlertToast('Select Franchise First');
        $(this).val("Select One");
        return false;
    }
    if($('#categoryList').val() == null){
        showAlertToast('Select Category');
        $(this).val("Select One");
        return false;
    }
    
     let formData = new FormData();
     formData.append("admin_uuid", localStorage.getItem('admin_uuid'));
    
     $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/get_product",
        "method": "POST",
        "data": formData,
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        success: (result) => {

            let objFull = JSON.parse(result).data.Approved;
            
            //console.log(objFull)
            $("#productList").empty();
            $("#productList").append('<option selected disabled>Select One</option>');
            $.each(objFull, (key, value) => {

                    if($('#categoryList').val() == value.category && $(this).val() == value.brand){
                        $("#productList").append(`
                        <option value="${value.productId}">${value.product_name}</option>
                        `);
                    }
            });
            

        },
        error: (err) => {
            console.log(err.responseText);
        }
    })
 });

$(document).on('change','#franchiseSel',function () {
    const franchiseId = $(this).val();
    let formData = new FormData();
    formData.append("admin_uuid", localStorage.getItem('admin_uuid'));
    formData.append("franchise_uuid",franchiseId);
    $.ajax({
       "async": true,
       "crossDomain": true,
       "url": "https://patashalafinder.in/amrutha/get_product_from_franchise",
       "method": "POST",
       "data": formData,
       "processData": false,
       "contentType": false,
       "mimeType": "multipart/form-data",
       success: (result) => {

           const data = JSON.parse(result).data;
          
           const prdtList = data.product;
           //console.log(prdtList)
           $("#prdt_id_ofer").empty();
           $("#prdt_id_ofer").append('<option selected disabled>Select One</option>');
           $.each(prdtList, (key, value) => {

                   //console.log(value)
                   $("#prdt_id_ofer").append(`
                   <option value="${value.product_uuid}">${value.product_name}</option>
                   `);
           });
           

       },
       error: (err) => {
           console.log(err.responseText);
       }
   })
});
$(document).on('change','#franchiseList',function () {
    const franchiseId = $(this).val();
    let formData = new FormData();
    formData.append("admin_uuid", localStorage.getItem('admin_uuid'));
    formData.append("franchise_uuid",franchiseId);
    $.ajax({
       "async": true,
       "crossDomain": true,
       "url": "https://patashalafinder.in/amrutha/get_product_from_franchise",
       "method": "POST",
       "data": formData,
       "processData": false,
       "contentType": false,
       "mimeType": "multipart/form-data",
       success: (result) => {

        $("#appendTableFranchisePrdt").empty();
        $("#appendTableFranchisePrdt").append(`
        <table id="example-1" class="table table-bordered dt-responsive display" cellspacing="0"
                            width="100%">
                            <thead style="font-size: 11px;">
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <th>Product Details</th>
                                    <th>Image</th>
                                    <th>Listing Price</th>
                                    <th>Usual Price</th>
                                    <th>Final Price</th>
                                    <th>Stock</th>
                                    <th>Category</th>
                                    <th>Food Preference</th>
                                    <th>Brand</th>
                                    <th>Weight</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tfoot style="font-size:11px">
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <th>Product Details</th>
                                    <th>Image</th>
                                    <th>Listing Price</th>
                                    <th>Usual Price</th>
                                    <th>Final Price</th>
                                    <th>Stock</th>
                                    <th>Category</th>
                                    <th>Food Preference</th>
                                    <th>Brand</th>
                                    <th>Weight</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>

                            <tbody id="appendAllAddedProduct">

                            </tbody>
                        </table>
        `);
           const data = JSON.parse(result).data;
          
           const prdtList = data.product;
           $.each(prdtList, (key, value) => {
                   $("#appendAllAddedProduct").append(`
                   <tr id="`+value.offer_uuid+`">
                        <td><input type="checkbox"></td>
                        <td>${value.product_name}</td>
                        <td><img src="https://patashalafinder.in/media/` + value.product_image_1_web + `" height=30px width=30px></td>
                        <td >${value.mrp}</td>
                        <td >${value.mrp}</td>
                        <td>${value.selling_price}</td>
                        <td>Unknown</td>
                        <td>${value.category}</td>
                        <td>${value.food_preference}</td>
                        <td>${value.brand}</td>
                        <td>${value.quantity} gm</td>
                        <td><span><i class="fa fa-edit"></i></span></td>
           </tr>
                   `);
              

           });
           $.getScript('assets/js/new.js')

       },
       error: (err) => {
           console.log(err.responseText);
       }
   })
});
$(document).on('change','#franchiseHot',function () {
    const franchiseId = $(this).val();
    let formData = new FormData();
    formData.append("admin_uuid", localStorage.getItem('admin_uuid'));
    formData.append("franchise_uuid",franchiseId);
    $.ajax({
       "async": true,
       "crossDomain": true,
       "url": "https://patashalafinder.in/amrutha/get_product_from_franchise",
       "method": "POST",
       "data": formData,
       "processData": false,
       "contentType": false,
       "mimeType": "multipart/form-data",
       success: (result) => {

           const data = JSON.parse(result).data;
          
           const prdtList = data.product;
           //console.log(prdtList)
           $("#prdt_sub_cat_hot").empty();
           $("#prdt_sub_cat_hot").append('<option selected disabled>Select One</option>');
           $.each(prdtList, (key, value) => {

               
                   //console.log(value)
                   $("#prdt_sub_cat_hot").append(`
                   <option value="${value.product_uuid}">${value.product_name}</option>
                   `);
              

           });
           

       },
       error: (err) => {
           console.log(err.responseText);
       }
   });

   $.ajax({
    url:"https://patashalafinder.in/amrutha/get_hotdeal",
    type:"POST",
    dataType:"JSON",
    processData: false,
    contentType: false,
    data:formData,
    success: (result) => {

        $("#appendHotOfferTable").empty()
                $("#appendHotOfferTable").append(`
                <label>Added Hot Offer For This Franchise</label>
                <table id="example-1" class="table table-striped dt-responsive display" cellspacing="0" width="100%" style="font-size:12px">
                            <thead>
                                <tr>
                                    <th class="sorting_disabled">Type</th>
                                    <th>Product Name</th>
                                    <th>Food Preference</th>
                                    <th>Status</th>
                                    <th>Added On</th>
                                    <th>Active From</th>
                                    <th>Expiring On</th>
                                    <th>Amount/Percent</th>
                                    <th>Stock Alloted</th>
                                    <th>Stock Left</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tfoot>
                                <tr>
                                    <th class="sorting_disabled">Type</th>
                                    <th>Product Name</th>
                                    <th>Food Preference</th>
                                    <th>Status</th>
                                    <th>Added On</th>
                                    <th>Active From</th>
                                    <th>Expiring On</th>
                                    <th>Amount/Percent</th>
                                    <th>Stock Alloted</th>
                                    <th>Stock Left</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>

                            <tbody id="appendHotOfferTableAdmin">

                            </tbody>
                    </table>
                `);
        //console.log(result);
        $.each(result.data,(key,value)=>{
            $("#appendHotOfferTableAdmin").append(`
            <tr id="`+value.hotdeal_uuid+`">
                    <td>${value.hotdeal_type}</td>
                    <td>`+value.product_name+`</td>
                    <td>${value.food_preference}</td>
                    <td>`+value.hotdeal_status+`</td>
                    <td >${value.added_date.split("T")[0]}</td>
                    <td >${value.hotdeal_start_date.split("T")[0]} ${value.hotdeal_start_time}</td>
                    <td>${value.hotdeal_end_date.split("T")[0]} ${value.hotdeal_end_time}</td>
                    <td>${value.hotdeal_percent == null ? value.hotdeal_amount : value.hotdeal_percent}</td>
                    <td>${value.stock}</td>
                    <td>${value.available_stock}</td>
                    <td><span class="editCartOffer"><i class="fa fa-edit"></i></span></td>
            </tr>
                
        `);
        })
    
    },
    error: (err) => {
        console.log(err.responseText.message);
        
    }
})
});

$(document).on('click',"input[name='hotOffer']",function () {
    if($("input[name='hotOffer']:checked").val() == "PERCENT"){
        $("#amountHot").hide()
        $("#percentHot").show()
    }else{
        $("#percentHot").hide()
        $("#amountHot").show()
        
    }
})
$(document).on('change','#productList',function () {
    const franchiseId = $('#franchiseList').val();
    if($('#franchiseList').val() == null){
        showAlertToast('Select Franchise First');
        $(this).val("Select One");
        return false;
    }
    if($('#categoryList').val() == null){
        showAlertToast('Select Category');
        $(this).val("Select One");
        return false;
    }
    if($('#brandList').val() == null){
        showAlertToast('Select Brand');
        $(this).val("Select One");
        return false;
    }
    
    let formData = new FormData();
    formData.append("admin_uuid", localStorage.getItem('admin_uuid'));
    formData.append("category_name",$('#categoryList').val());
    formData.append("brand_name",$('#brandList').val());
    formData.append("productId",$(this).val());
    $.ajax({
       "async": true,
       "crossDomain": true,
       "url": "https://patashalafinder.in/amrutha/filter_batch_to_franchise",
       "method": "POST",
       "data": formData,
       "processData": false,
       "contentType": false,
       "mimeType": "multipart/form-data",
       success: (result) => {

           const data = JSON.parse(result).data.batch_list;
         
           $(".select2-search-choice-close").trigger('click');
           $("#s2example-2").empty();
           $("#s2example-2").append('<optgroup label="Batch" id="appendOption"></optgroup>')
          
           $("#batchList").empty();
           $("#batchList").append('<option selected disabled>Select One</option>');
           $.each(data, (key, value) => {

                    $("#appendOption").append(`<option >${value.batchUUID}</option>`)
                  
                   $("#batchList").append(`
                   <option value="${value.batchUUID}">${value.batchUUID}</option>
                   `);
              
           });
           

       },
       error: (err) => {
        const data = JSON.parse(err.responseText).message;
        showAlertToast(data)
       }
   })
});

$(document).on('click','#allotToFranchise',function () {
    if($('#franchiseList').val() == null){
        showAlertToast("Select Franchise.");
        return false
    }
    if($('#categoryList').val() == null){
        showAlertToast("Select Category.");
        return false
    }
    if($('#brandList').val() == null){
        showAlertToast("Select Brand.");
        return false
    }
    if($('#productList').val() == null){
        showAlertToast("Select Product.");
        return false
    }
    let formData = new FormData();
    formData.append("admin_uuid", localStorage.getItem('admin_uuid'));
    formData.append("franchise_uuid", $('#franchiseList').val());
    formData.append("category_name",$('#categoryList').val());
    formData.append("brand_name",$('#brandList').val());
    formData.append("productId",$("#productList").val());
    
    let batchArray =[];
    batchArray =  $("#s2example-2").val() != null ? $("#s2example-2").val().map((batchid)=>{return batchid}) : [];
    
    if(batchArray.length == 0){
        showAlertToast("No Batch Present.");
        return false
    }
    formData.append("batch_list",JSON.stringify(batchArray));
   
    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://patashalafinder.in/amrutha/add_product_to_franchise",
        "method": "POST",
        "data": formData,
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        success: (result) => {
            const data = JSON.parse(result).message;
           $("#eco_products_allot").trigger('click')
           showAlertToast(data)
        },
        error: (err) => {
            const data = JSON.parse(err.responseText).message;
            showAlertToast(data)
        }
    })
});

let franchiseEventArr = [];

function displayEventImage(input) {
    //console.log(headerOrSideImage)
    if(input.files && input.files[0]){
        let reader = new FileReader();

        reader.onload =function (e) {
            var image = new Image();
            image.src = e.target.result;
        
            image.onload = function() {
              
                        // if(this.width / this.height >= 2){
                            $("#imageevents").empty();
                            $("#imageevents").append(`<img src="${this.src}" width="100%" height="100%">`);
                            $('.image_events_web').find('div').not('#imageevents').remove();
                            $(".image_events_web").append(`
                            <div class="image_upload_events imageevents">
                            <span class="uploadIcon"><img src="data/addBadge.png" width="100px" height="100px" alt=""></i></span>
                
                            </div>
                            `)
                       
                
            };
            
           
        }
        
        reader.readAsDataURL(input.files[0])
    }
    
}

$(document).on('change','#trigger_event_image_upload',function(e){
   
    let filename=$("#trigger_event_image_upload").get(0);
    let thatfile=filename.files;
    franchiseEventArr[0]=thatfile[0]
    //console.log(structureSliderArr);
    displayEventImage(this);
    
});

$(document).on('click','.image_events_web',function(){
    
    $('#trigger_event_image_upload').trigger('click');
});

$(document).on('click','#addEvents',function () {
    if($("#eventTitle").val() == ""){
        showAlertToast("Set Event Title.");
        return false;
    }
    else if($("#event_start_date").val() == ""){
        showAlertToast("Set Event Start Date.");
        return false;
    }
    else if($("#eventStartTime").val() == ""){
        showAlertToast("Set Event Start Time.");
        return false;
    }
    else if($("#eventEndTime").val() == ""){
        showAlertToast("Set Event End Time.");
        return false;
    }
    else if($("#eventHeadlines").val() == ""){
        showAlertToast("Set Event Headlines.");
        return false;
    }
    else if($("#eventAddress").val() == ""){
        showAlertToast("Set Event Address.");
        return false;
    }
    else if($("#noOfSeats").val() == ""){
        showAlertToast("Set Number Of Seats.");
        return false;
    }
    else if($("#noOfSpeakers").val() == ""){
        showAlertToast("Set Number Of Speakers.");
        return false;
    }
    else if($("#eventDescription").val() == ""){
        showAlertToast("Set Events Description.");
        return false;
    }
    else if($("#mapLink").val() == ""){
        showAlertToast("Give Events Location Link.");
        return false;
    }
    else if($("#event_display_from_date").val() == ""){
        showAlertToast("Set Events Display Date.");
        return false;
    }
    else if(franchiseEventArr.length == 0){
        showAlertToast("Select Event Image.");
        return false;
    }
    let form = new FormData();
    form.append("admin_uuid", localStorage.getItem('admin_uuid'));
    form.append("event_title", $("#eventTitle").val());
    form.append("event_date", $("#event_start_date").val());
    form.append("event_start_time", $("#eventStartTime").val());
    form.append("event_end_time", $("#eventEndTime").val());
    form.append("event_headlines", $("#eventHeadlines").val());
    form.append("event_address", $("#eventAddress").val());
    form.append("no_of_seats", $("#noOfSeats").val());
    form.append("no_of_speakers", $("#noOfSpeakers").val());
    form.append("event_description", $("#eventDescription").val());
    form.append("location_latitude", $("#locationLatitude").val());
    form.append("location_longitude", $("#locationLongitude").val());
    form.append("location_map_link", $("#mapLink").val());
    form.append("display_from_date", $("#event_display_from_date").val()+" "+"00:00");
    form.append("event_image", franchiseEventArr[0]);

    // return false;

    $.ajax({
        url:"https://patashalafinder.in/amrutha/add_amruthashala_events",
        type:"POST",
        dataType:"JSON",
        processData: false,
        contentType: false,
        data:form,
        success: (result) => {
            // console.log(result.message);
            franchiseEventArr = []
             $(".form-control").val('');
            showAlertToast(result.message);
            $("#eco-events-add").trigger('click')
        },
        error: (err) => {
            // console.log(err.responseText);
            showAlertToast(err.responseText)
        }
    })
})
let issueuuid ='';
let userid='';

$(document).on('click','.viewMail',function () {
    $("#mailListCollapse").removeClass('col-md-12').addClass('col-md-6');
    $("#mailListExpand").show();
    let thisVal = $(this).attr('class').split(" ");
    const issue_id = thisVal[2];
    const user_id = thisVal[3];
    userid = user_id
    issueuuid = issue_id;
    $.ajax({
        url:"https://patashalafinder.in/amrutha/get_user_issues_details",
        type:"POST",
        dataType:"JSON",
        data:{
            userId:user_id,
            issue_uuid:issue_id
        },
        success:function (result) { 
            // console.log(result);
            let data = result.data.activities;
            //console.log(data);
            $("#appendChatIssue").empty();
            $.each(data,(key,value)=>{
                if(value.replied_by == "admin"){
                    $("#appendChatIssue").append(`
                    <div class="toDiv">
                        <p class="to">${value.message}</p>
                    </div>
                    `);
                   
                }else if(value.replied_by == "user"){
                    $("#appendChatIssue").append(`
                    <div class="fromDiv">
                        <p class="from">${value.message}</p>
                    </div>
                    `);
                }
            });
            $('#appendChatIssue').scrollTop($('#appendChatIssue')[0].scrollHeight + 100);
        },
        error: function (err) {
            console.log(err.responseText)
        }
    });
});

$(document).on('keypress','#sendResponse',function(e) {
    if(issueuuid == ""){
        showAlertToast("Issue Id is not present.");
        return false
    }
    if(e.which == 13) {
        
        $.ajax({
            url:"https://patashalafinder.in/amrutha/reply_for_user_order_issues",
            type:"POST",
            dataType:"JSON",
            data:{
               
                admin_uuid:localStorage.getItem('admin_uuid'),
                issue_uuid:issueuuid,
                issue_description: $("#sendResponse").val()
            },
            success:function (result) { 
                
                //console.log(result);
                $.ajax({
                    url:"https://patashalafinder.in/amrutha/get_user_issues_details",
                    type:"POST",
                    dataType:"JSON",
                    data:{
                        userId:userid,
                        issue_uuid:issueuuid
                    },
                    success:function (result) { 
                        // console.log(result);
                        let data = result.data.activities;
                        //console.log(data);
                        $("#appendChatIssue").empty();
                        $.each(data,(key,value)=>{
                            if(value.replied_by == "admin"){
                                $("#appendChatIssue").append(`
                                <div class="toDiv">
                                    <p class="to">${value.message}</p>
                                </div>
                                `);
                                
                            }else if(value.replied_by == "user"){
                                $("#appendChatIssue").append(`
                                <div class="fromDiv">
                                    <p class="from">${value.message}</p>
                                </div>
                                `);
                            }
                        });
                    },
                    error: function (err) {
                        console.log(err.responseText)
                    }
                });
                
            },
            error: function (err) {
                console.log(err.responseText)
            }
        });
        $(this).val('');
       
        setTimeout(()=>{
            
            $('#appendChatIssue').scrollTop($('#appendChatIssue')[0].scrollHeight + 100);
        },400)
        
    }
    
});


function showAlertToast(text){
    var alert = $(".alert-container");

    $(".spanAlert").text(text);
   
    alert.slideDown();
    window.setTimeout(function() {
      alert.slideUp();
    }, 3000);
  
}




//##################------TO REDIRECT LINKS TO EXTERNAL BROWSER----############################
const shell = require('electron').shell;
// assuming $ is jQuery
$(document).on('click', 'a[class="externalLink"]', function(event) {
    event.preventDefault();
    shell.openExternal(this.href);
});


//###################################-----All Code Above This-------##########################################

const remote = require('electron').remote;
document.getElementById("closeBtn").addEventListener("click", function (e) {
    var window = remote.getCurrentWindow();
    window.close();
});
document.getElementById("minimizeBtn").addEventListener("click", function (e) {
    var window = remote.getCurrentWindow();
    window.minimize();
});